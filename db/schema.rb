# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170723012229) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "approvals", force: :cascade do |t|
    t.string "approvable_type"
    t.bigint "approvable_id", null: false
    t.bigint "approver_id", null: false
    t.datetime "approved_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["approvable_type", "approvable_id"], name: "index_approvals_on_approvable_type_and_approvable_id"
    t.index ["approver_id"], name: "index_approvals_on_approver_id"
  end

  create_table "budgets", force: :cascade do |t|
    t.float "total", null: false
    t.float "remaining"
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "budgetable_type"
    t.bigint "budgetable_id", null: false
    t.string "name"
    t.index ["budgetable_type", "budgetable_id"], name: "index_budgets_on_budgetable_type_and_budgetable_id"
  end

  create_table "dues", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.float "amount", null: false
    t.text "description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_dues_on_organization_id"
  end

  create_table "event_invites", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.bigint "user_id", null: false
    t.datetime "rsvp_at"
    t.datetime "checked_in_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_invites_on_event_id"
    t.index ["user_id"], name: "index_event_invites_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "created_by_user_id", null: false
    t.string "name", null: false
    t.datetime "occurs_at", null: false
    t.float "location_lat"
    t.float "location_long"
    t.integer "required_points"
    t.boolean "alcohol"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "venue_name"
    t.string "venue_address"
    t.string "venue_city"
    t.string "venue_state"
    t.string "venue_zipcode"
    t.float "achievable_points"
    t.bigint "coordinator_id"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.index ["coordinator_id"], name: "index_events_on_coordinator_id"
    t.index ["created_by_user_id"], name: "index_events_on_created_by_user_id"
    t.index ["organization_id"], name: "index_events_on_organization_id"
  end

  create_table "form_submissions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "form_id", null: false
    t.string "status", default: "pending", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.index ["form_id"], name: "index_form_submissions_on_form_id"
    t.index ["user_id"], name: "index_form_submissions_on_user_id"
  end

  create_table "forms", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "approver_roles_id", null: false, array: true
    t.string "name", null: false
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.index ["approver_roles_id"], name: "index_forms_on_approver_roles_id"
    t.index ["organization_id"], name: "index_forms_on_organization_id"
  end

  create_table "meeting_notes", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.text "notes", default: "", null: false
    t.datetime "occurred_at", null: false
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.index ["organization_id"], name: "index_meeting_notes_on_organization_id"
  end

  create_table "message_groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type", null: false
    t.bigint "organization_id"
    t.index ["organization_id"], name: "index_message_groups_on_organization_id"
  end

  create_table "message_groups_users", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "message_group_id", null: false
    t.boolean "has_unread_messages", default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_group_id"], name: "index_message_groups_users_on_message_group_id"
    t.index ["user_id"], name: "index_message_groups_users_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "from_user_id", null: false
    t.bigint "message_group_id", null: false
    t.text "message", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["from_user_id"], name: "index_messages_on_from_user_id"
    t.index ["message_group_id"], name: "index_messages_on_message_group_id"
  end

  create_table "organization_links", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.string "link", null: false
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_organization_links_on_organization_id"
  end

  create_table "organization_user_roles", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "user_id", null: false
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role_id"
    t.index ["organization_id"], name: "index_organization_user_roles_on_organization_id"
    t.index ["user_id"], name: "index_organization_user_roles_on_user_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "organizable_type"
    t.integer "organizable_id"
    t.string "name", null: false
    t.string "type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.index ["organizable_type", "organizable_id"], name: "index_organizations_on_organizable_type_and_organizable_id"
  end

  create_table "role_permissions", force: :cascade do |t|
    t.bigint "role_id", null: false
    t.string "allow", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["allow"], name: "index_role_permissions_on_allow"
    t.index ["role_id"], name: "index_role_permissions_on_role_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "title", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "default_name"
    t.index ["default_name"], name: "index_roles_on_default_name"
  end

  create_table "tasks", force: :cascade do |t|
    t.bigint "assigned_to_user_id", null: false
    t.bigint "assigned_by_user_id", null: false
    t.datetime "due_at", null: false
    t.text "notes", default: "", null: false
    t.string "task_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "completed_at"
    t.index ["assigned_by_user_id"], name: "index_tasks_on_assigned_by_user_id"
    t.index ["assigned_to_user_id"], name: "index_tasks_on_assigned_to_user_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.string "transactable_type"
    t.bigint "transactable_id", null: false
    t.bigint "created_by_user_id", null: false
    t.datetime "occurred_at", null: false
    t.float "amount", null: false
    t.text "notes"
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.index ["created_by_user_id"], name: "index_transactions_on_created_by_user_id"
    t.index ["transactable_type", "transactable_id"], name: "index_transactions_on_transactable_type_and_transactable_id"
  end

  create_table "user_grades", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "course", null: false
    t.string "grade", null: false
    t.string "notes"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_grades_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.datetime "date_of_birth"
    t.string "major"
    t.string "cellphone"
    t.string "facebook_url"
    t.string "instagram_url"
    t.string "auth_token"
    t.string "device_token"
    t.string "device_type"
    t.string "hometown"
    t.string "schedule_url"
    t.float "previous_semester_gpa"
    t.float "cumulative_gpa"
    t.string "emergency_contact_name"
    t.string "emergency_contact_number"
    t.string "status"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.string "clubs_orgs"
    t.string "year_in_school"
    t.index ["auth_token"], name: "index_users_on_auth_token"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
