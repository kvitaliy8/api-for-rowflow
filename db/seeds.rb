
# Update the default set of roles to the db that are defined in config/default_roles.yml
Role.update_defaults

# CREATE A SORORITY, CHAPTER, AND COMMITTEE
sorority = Sorority.find_or_create_by(name: "Sorority TEST")
chapter = Chapter.find_or_create_by(name: "Chapter TEST", organizable: sorority)
committee = Committee.find_or_create_by(name: "Committee TEST", organizable: chapter)

# CREATE SUPER ADMIN TEST USER
sa_user = User.find_by_email("8b47651e@example.com")
if not sa_user.present?
	sa_user = User.create(email: "8b47651e@example.com", password: "069cea18ac1b", password_confirmation: "069cea18ac1b", first_name: "SA TESTY", last_name: "McTesterson", date_of_birth: "1994-03-28 08:30:00.0", major: "Computer Science", cellphone: "8675309")
end
sa_role = Role.find_by_default_name("super_admin")
OrganizationUserRole.find_or_create_by(user: sa_user, organization: sorority, role: sa_role)

# CREATE COLLEGIATE TEST USER
c_user = User.find_by_email("069cea18ac1b@example.com")
if not c_user.present?
	c_user = User.create(email: "069cea18ac1b@example.com", password: "8b47651e", password_confirmation: "8b47651e", first_name: "C Testy", last_name: "McTesterson", date_of_birth: "1994-03-28 08:30:00.0", major: "Computer Science", cellphone: "8675309")
end
c_role = Role.find_by_default_name("collegiate")
OrganizationUserRole.find_or_create_by(user: c_user, organization: chapter, role: c_role)
c_role2 = Role.find_by_default_name("spirit_chair")
OrganizationUserRole.find_or_create_by(user: c_user, organization: committee, role: c_role2)

# CREATE BUDGETS FOR EACH OF THE ORGANIZATIONS
s_budget = sorority.budgets.find_or_create_by(name: "Annual Budget", total: 10000, remaining: 9000)
ch_budget = chapter.budgets.find_or_create_by(name: "Annual Budget", total: 1000, remaining: 900)
co_budget = committee.budgets.find_or_create_by(name: "Annual Budget", total: 100, remaining: 90)

# CREATE LINKS FOR EACH OF THE ORGANIZATIONS
s_link = sorority.organization_links.find_or_create_by(name: "Big Time Link", link: "https://www.google.com", description: "How many ducks?")
ch_link = chapter.organization_links.find_or_create_by(name: "Medium Link", link: "https://www.google.com", description: "How many ducks?")
co_link = committee.organization_links.find_or_create_by(name: "Small Link", link: "https://www.google.com", description: "How many ducks?")

# CREATE A SUPER ADMIN BUDGET TRANSACTIONS
s_budget.budget_transactions.find_or_create_by(amount: 1000, notes: "Expensive Sorority Advertising", created_by_user: sa_user,  occurred_at: "2017-03-28 08:30:00.0")
ch_budget.budget_transactions.find_or_create_by(amount: 100, notes: "Expensive Chapter Advertising", created_by_user: sa_user,  occurred_at: "2017-03-28 08:30:00.0")
co_budget.budget_transactions.find_or_create_by(amount: 10, notes: "Expensive Committee Advertising", created_by_user: sa_user,  occurred_at: "2017-03-28 08:30:00.0")

# START A DM BETWEEN THE TWO USERS
dm = DirectMessage.create()
dm.message_groups_users.find_or_create_by(user: sa_user, has_unread_messages: true)
dm.message_groups_users.find_or_create_by(user: c_user, has_unread_messages: true)
dm.messages.find_or_create_by(from_user: sa_user, message: "Hey C Testy...")
dm.messages.find_or_create_by(from_user: c_user, message: "Hey SA Testy...")

# CREATE CHANNELS FOR EACH OF THE ORGANIZATIONS
s_channel = sorority.channels.find_or_create_by(name: "Sorority TEST Channel")
s_sa_message = s_channel.messages.find_or_create_by(message: "HERE I AM!", from_user: sa_user)
s_c_message = s_channel.messages.find_or_create_by(message: "ME TOO!", from_user: c_user)

ch_channel = chapter.channels.find_or_create_by(name: "Chapter TEST Channel")
ch_sa_message = ch_channel.messages.find_or_create_by(message: "HERE I AM!", from_user: sa_user)
ch_c_message = ch_channel.messages.find_or_create_by(message: "ME TOO!", from_user: c_user)

co_channel = committee.channels.find_or_create_by(name: "Committee TEST Channel")
co_sa_message = co_channel.messages.find_or_create_by(message: "HERE I AM!", from_user: sa_user)
co_c_message = co_channel.messages.find_or_create_by(message: "ME TOO!", from_user: c_user)

# CREATE TASKS FOR BOTH USERS
Task.find_or_create_by(assigned_to_user: sa_user, assigned_by_user: c_user, due_at: "2018-03-28 08:30:00.0", notes: "SA, Please Complete this TEST Task.")
Task.find_or_create_by(assigned_to_user: c_user, assigned_by_user: sa_user, due_at: "2018-03-28 08:30:00.0", notes: "C, Please Complete this TEST Task.")

# CREATE CHAPTER/COMMITTEE MEETING NOTES (MINUTES)
MeetingNote.find_or_create_by(organization: sorority, name: "Executive Meeting", occurred_at: "2017-03-28 08:30:00.0", notes: "Such a great executive meeting guys.")
MeetingNote.find_or_create_by(organization: chapter, name: "Chapter", occurred_at: "2017-03-28 08:30:00.0", notes: "Such a great meeting guys.")
MeetingNote.find_or_create_by(organization: committee, name: "Spirit Board Meeting", occurred_at: "2017-03-28 08:30:00.0", notes: "Such a great spirit guys.")

# CREATE POINTS TRANSACTIONS FOR USERS
PointsTransaction.find_or_create_by(transactable: c_user, created_by_user: sa_user, occurred_at: "2017-03-28 08:30:00.0", amount: 500, notes: "GOOD JOB!")
PointsTransaction.find_or_create_by(transactable: sa_user, created_by_user: c_user, occurred_at: "2017-03-28 08:30:00.0", amount: 500, notes: "GOOD JOB!")

# CREATE EVENT FOR USERS TO ATTEND
event = Event.find_or_create_by(organization: chapter, created_by_user: c_user, name: "Party!!", occurs_at: "2017-07-28 08:30:00.0", location_lat: 32.7765, location_long: -79.9311, required_points: 30, alcohol: true, venue_name: "Royal American", venue_address: "970 Morrison Dr", venue_city: "Charleston", venue_state: "SC", venue_zipcode: "29403", achievable_points: 100)
sa_invite = EventInvite.find_or_create_by(event: event, user: sa_user)
c_invite = EventInvite.find_or_create_by(event: event, user: c_user)

# CREATE ORG FORM
form = Form.find_or_create_by(organization: sorority, approver_roles_id: [c_role2.id], name: "TEST FORM", notes: "Please complete")

# CREATE USER GRADES
sa_grade = UserGrade.find_or_create_by(user: sa_user, course: 'Biology', grade: 'A', notes: "Sorry I took so long!")
c_grade = UserGrade.find_or_create_by(user: c_user, course: 'Biology', grade: 'B', notes: "I got a B :(")

# CREATE ORG DUES
dues = Due.find_or_create_by(organization: sorority, amount: 1000, description: "Annual")

