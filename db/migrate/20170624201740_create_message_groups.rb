class CreateMessageGroups < ActiveRecord::Migration[5.1]

	def change

		create_table :message_groups do |t|
			t.references :users,				null: false, array: true
			t.references :unread_by_users, 		null: false, array: true, default: []
			t.references :deleted_by_users, 	null: false, array: true, default: []
			t.string	 :name
			t.timestamps 						null: false
		end

	end
	
end
