class CreateApprovals < ActiveRecord::Migration[5.1]

	def change

		create_table :approvals do |t|
			t.references	:approvable,	null: false, polymorphic: true, index: true
			t.references	:approver,		null: false, index: true
			t.datetime		:approved_at
			t.timestamps 					null: false
		end

		remove_column :events, :approved_by_user_id, :integer
		remove_column :form_submissions, :approved_by_users_id, :integer, array: true
		remove_column :transactions, :approved_by_user_id, :integer

	end

end