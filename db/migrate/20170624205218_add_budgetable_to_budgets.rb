class AddBudgetableToBudgets < ActiveRecord::Migration[5.1]

	def change
		
		change_table :budgets do |t|
			t.references	:budgetable,	null: false, polymorphic: true, index: true
		end
	
	end

end
