class AddAttachmentAttachmentToMeetingNotes < ActiveRecord::Migration[5.1]
  def self.up
    change_table :meeting_notes do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :meeting_notes, :attachment
  end
end
