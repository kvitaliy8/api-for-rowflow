class AddMoreFieldsToUsers < ActiveRecord::Migration[5.1]

	def change

		change_table :users do |t|
			t.string 	:hometown
			t.string 	:image_url
			t.string 	:schedule_url
			t.float		:previous_semester_gpa
			t.float		:cumulative_gpa
			t.string	:emergency_contact_name
			t.string	:emergency_contact_number
			t.string	:status
		end

	end

end