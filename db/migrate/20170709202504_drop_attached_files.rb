class DropAttachedFiles < ActiveRecord::Migration[5.1]

	def change
		drop_table :attached_files

		remove_column :users, :image_url
		add_attachment :users, :image

		add_attachment :tasks, 				:attachment
		add_attachment :events, 			:attachment
		add_attachment :transactions, 		:attachment
		add_attachment :forms, 				:attachment
		add_attachment :form_submissions, 	:attachment
	end

end
