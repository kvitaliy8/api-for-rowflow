class CreateTasks < ActiveRecord::Migration[5.1]
  
	def change

		create_table :tasks do |t|
			t.references	:assigned_to_user, 	null: false, index: true
			t.references	:assigned_by_user,	null: false, index: true
			t.string		:status,			null: false, default: "pending"
			t.datetime		:due_at,			null: false
			t.text			:notes,				null: false, default: ""
			t.string		:type
			t.timestamps 						null: false
		end

	end

end
