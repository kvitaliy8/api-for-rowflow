class CreateRolePermissions < ActiveRecord::Migration[5.1]

  	def change

		create_table :role_permissions do |t|
			t.references	:role,	null: false, index: true
			t.string		:allow, null: false, index: true
			t.timestamps
		end

	end

end
