class SplitMessageGroups < ActiveRecord::Migration[5.1]

  	def change

  		change_table :message_groups do |t|
			t.string 		:type, null: false
			t.references 	:organization
		end

  	end

end
