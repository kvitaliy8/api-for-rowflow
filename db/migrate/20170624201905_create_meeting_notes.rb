class CreateMeetingNotes < ActiveRecord::Migration[5.1]
	
	def change
	
		create_table :meeting_notes do |t|
			t.references	:organization,		null: false, index: true
			t.text			:notes,				null: false, default: ""
			t.datetime		:occurred_at,		null: false
			t.string		:name
			t.timestamps 						null: false
		end
	
	end

end
