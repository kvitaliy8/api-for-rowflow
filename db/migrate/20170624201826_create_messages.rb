class CreateMessages < ActiveRecord::Migration[5.1]

	def change

		create_table :messages do |t|
			t.references 	:from_user,		null: false, index: true
			t.references 	:message_group,	null: false, index: true
			t.text			:message,		null: false
			t.timestamps 					null: false
		end

	end

end
