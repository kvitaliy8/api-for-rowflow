class CreateFormSubmissions < ActiveRecord::Migration[5.1]

  def change

		create_table :form_submissions do |t|
			t.references	:user,				null: false, index: true
			t.references	:form,				null: false, index: true
			t.string		:status,			null: false, default: "pending"
			t.references	:approved_by_users,	null: false, array: true, default: []
			t.timestamps 						null: false
		end

  end

end
