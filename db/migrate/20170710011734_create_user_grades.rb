class CreateUserGrades < ActiveRecord::Migration[5.1]

	def change

		create_table :user_grades do |t|
			t.references 	:user, 		null: false, index: true
			t.string		:course,	null: false
			t.string		:grade,		null: false
			t.string		:notes
			t.attachment	:attachment

			t.timestamps				null: false
		end
	end

end
