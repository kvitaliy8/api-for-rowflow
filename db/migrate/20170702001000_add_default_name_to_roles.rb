class AddDefaultNameToRoles < ActiveRecord::Migration[5.1]
  def change
		add_column :roles, :default_name, :string
		add_index :roles, :default_name
  end
end
