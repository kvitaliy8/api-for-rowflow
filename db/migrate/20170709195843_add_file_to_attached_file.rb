class AddFileToAttachedFile < ActiveRecord::Migration[5.1]

	def change
		remove_column :attached_files, :location_url
		add_attachment :attached_files, :file
	end

end
