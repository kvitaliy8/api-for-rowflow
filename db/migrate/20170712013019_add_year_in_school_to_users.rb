class AddYearInSchoolToUsers < ActiveRecord::Migration[5.1]

	def change
		add_column :users, :year_in_school, :string
	end

end
