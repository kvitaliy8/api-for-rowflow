class CreateRoles < ActiveRecord::Migration[5.1]
	
	def change
	
		create_table :roles do |t|
			t.references	:organization,	null: false, index: true
			t.references	:user,			null: false, index: true
			t.string		:name,			null: false
			t.string		:permissions, 	null: false, array: true, default: []
			t.datetime		:expires_at
			t.timestamps 					null: false
		end
	
	end

end
