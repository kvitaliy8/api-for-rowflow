class CreateOrganizations < ActiveRecord::Migration[5.1]

	def change

		create_table :organizations do |t|
			t.references	:organizable,	null: false, polymorphic: true, index: true
			t.string 		:name, 			null: false
			t.string 		:type,			null: false
			t.timestamps 					null: false
		end
		
	end

end
