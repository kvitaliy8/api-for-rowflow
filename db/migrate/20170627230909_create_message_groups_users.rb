class CreateMessageGroupsUsers < ActiveRecord::Migration[5.1]
	def change
		create_table :message_groups_users do |t|
			t.references 	:user,				null: false, index: true
			t.references 	:message_group,		null: false, index: true
			t.boolean		:has_unread_chats,  null: false, default: false
			t.datetime		:deleted_at
			t.timestamps						null: false
		end

		remove_column :message_groups, :users_id
		remove_column :message_groups, :deleted_by_users_id
		remove_column :message_groups, :unread_by_users_id
	end
end
