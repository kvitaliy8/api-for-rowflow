class CreateOrganizationLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :organization_links do |t|
      t.references 	:organization, 	null: false, index: true
	  t.string		:link,			null: false
	  t.string		:name,			null: false
	  t.text		:description
      t.timestamps
    end
  end
end
