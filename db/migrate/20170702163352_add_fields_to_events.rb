class AddFieldsToEvents < ActiveRecord::Migration[5.1]

	def change

		change_table :events do |t|
			t.string 		:venue_name
			t.string 		:venue_address
			t.string 		:venue_city
			t.string 		:venue_state
			t.string 		:venue_zipcode
			t.float 		:achievable_points
			t.references 	:coordinator
		end

	end

end