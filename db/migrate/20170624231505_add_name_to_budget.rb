class AddNameToBudget < ActiveRecord::Migration[5.1]

	def change
		
		change_table :budgets do |t|
			t.string :name
		end
	end

end
