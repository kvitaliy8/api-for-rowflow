class CreateTransactions < ActiveRecord::Migration[5.1]
	
	def change
	
		create_table :transactions do |t|
			t.references	:transactable,		null: false, polymorphic: true, index: true
			t.references	:created_by_user,	null: false, index: true
			t.references	:approved_by_user
			t.datetime		:occurred_at,		null: false
			t.float			:amount, 			null: false
			t.text			:notes
			t.datetime		:expires_at
			t.timestamps 						null: false
		end
	
	end

end
