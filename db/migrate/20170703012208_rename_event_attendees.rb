class RenameEventAttendees < ActiveRecord::Migration[5.1]

	def change
		rename_table :event_attendees, :event_invites
	end

end
