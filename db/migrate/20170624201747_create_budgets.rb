class CreateBudgets < ActiveRecord::Migration[5.1]
	
	def change
		
		create_table :budgets do |t|
			t.float			:total,			null: false
			t.float			:remaining
			t.datetime		:expires_at
			t.timestamps 					null: false
		end
	
	end

end
