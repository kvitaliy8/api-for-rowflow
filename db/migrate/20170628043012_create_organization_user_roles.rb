class CreateOrganizationUserRoles < ActiveRecord::Migration[5.1]

	def change
		rename_table 	:roles, 					:organization_user_roles
		remove_column 	:organization_user_roles, 	:name
		remove_column 	:organization_user_roles, 	:permissions

		create_table :roles do |t|
			t.string		:title,			null: false
			t.text			:description
			t.timestamps					null: false
		end

		add_column		:organization_user_roles, :role_id, :integer, index: true

	end

end
