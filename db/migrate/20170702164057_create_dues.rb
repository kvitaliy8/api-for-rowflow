class CreateDues < ActiveRecord::Migration[5.1]

	def change

		create_table :dues do |t|
			t.references 	:organization,		null: false, index: true
			t.float			:amount, 			null: false
			t.text			:description,		null: false
		  	t.timestamps						null: false
		end

	end

end
