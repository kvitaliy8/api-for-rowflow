class RemoveConstraintFromOrganizations < ActiveRecord::Migration[5.1]

	def change

		# Allow organizations to not belong to any other org (toplevel, ie sorority)
		change_column 	:organizations, 	:organizable_type, 		:string, 	null: true
		change_column 	:organizations, 	:organizable_id, 		:integer, 	null: true
	end

end
