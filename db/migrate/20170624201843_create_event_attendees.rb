class CreateEventAttendees < ActiveRecord::Migration[5.1]
	
	def change
	
		create_table :event_attendees do |t|
			t.references 	:event,			null: false, index: true
			t.references	:user,			null: false, index: true
			t.datetime		:rsvp_at
			t.datetime		:checked_in_at
			t.timestamps 					null: false
		end
	
	end

end
