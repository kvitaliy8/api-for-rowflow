class CreateAttachedFiles < ActiveRecord::Migration[5.1]
  
	def change

		create_table :attached_files do |t|
			t.references	:fileable,			null: false, polymorphic: true, index: true
			t.string		:location_url,		null: false
			t.string		:name,				null: false, default: ""
			t.timestamps 						null: false
		end

	end

end
