class ChangeTasksStatusToCompletedAt < ActiveRecord::Migration[5.1]

	def change
		remove_column :tasks, :status
		add_column :tasks, :completed_at, :datetime
	end

end
