class CreateForms < ActiveRecord::Migration[5.1]
	
	def change
		
		create_table :forms do |t|
			t.references 	:organization,		null: false, index: true
			t.references	:approver_roles, 	null: false, array: true
			t.string		:name,				null: false
			t.text			:notes
			t.timestamps 						null: false
		end
	
	end

end
