class RenameChatsToMessages < ActiveRecord::Migration[5.1]

	def change
		rename_column :message_groups_users, :has_unread_chats, :has_unread_messages
	end

end
