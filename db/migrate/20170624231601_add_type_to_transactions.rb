class AddTypeToTransactions < ActiveRecord::Migration[5.1]

	def change

		change_table :transactions do |t|
			t.string :type, null: false
		end
	end

end
