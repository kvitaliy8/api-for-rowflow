class AddFieldsToUsers < ActiveRecord::Migration[5.1]

	def change

		change_table :users do |t|
			t.string 	:first_name, 		null: false, default: ""
			t.string 	:last_name, 		null: false, default: ""
			t.datetime	:date_of_birth
			t.string	:major
			t.string	:cellphone
			t.string	:facebook_url
			t.string	:instagram_url
		end
	end

end
