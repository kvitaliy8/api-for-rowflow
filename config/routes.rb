require 'api_constraints'

Rails.application.routes.draw do

  # API ROUTES ---------------------------------------------------

  namespace :api, defaults: { format: :json } do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do

      post '/login',              to: 'sessions#create'
      post '/logout',             to: 'sessions#destroy'
      post '/reset_password',     to: 'users#reset_password'

      resources :users, only: [:index, :show, :update] do
        resources :tasks, only: [:index, :show, :create, :update] do
          post '/complete', to: 'tasks#complete', on: :member
          post '/incomplete', to: 'tasks#incomplete', on: :member
        end
        resources :points_transactions, only: [:index, :create]
      end

      resources :approvals, only: [:index] do
        post '/approve', to: 'approvals#approve', on: :member
      end

      resources :dues, only: [:index]
      resources :dues_transactions, only: [:index, :create]

      resources :event_invites, only: [:index] do
        post '/rsvp', to: 'event_invites#rsvp', on: :member
        post '/check_in', to: 'event_invites#check_in', on: :member
      end

      resources :meeting_notes, only: [:index, :show, :create]

      resources :channels, only: [:index] do
        resources :messages, only: [:index, :create]
      end

      resources :direct_messages, only: [:index, :create] do
        resources :message_groups_users, only: [:index, :create, :update]
        resources :messages, only: [:index, :create]
      end

      resources :budgets, only: [:index] do
        resources :budget_transactions, only: [:index, :create]
      end

      resources :user_grades, only: [:index, :show, :create]
    end
  end


  # WEB APP ROUTES ---------------------------------------------------

  root 'static_pages#index'
  get 'my_flow', to: 'users#my_flow'

  devise_for :users, path_names: { sign_in: :login, sign_out: :logout }

  resources :users, only: [:index, :show] do
    collection do
      get 'my_info', action: :my_info
      put 'update_my_avatar', action: :update_my_avatar
      put 'update_my_info', action: :update_my_info
    end
  end

  resources :user_grades, only: [:index, :create, :destroy]

  resources :points_transactions, only: [:index, :create]

  resources :tasks, only: [:index, :create] do
    post '/complete', to: 'tasks#complete', on: :member
  end

  resources :calendar, only: [:index, :show, :new, :create] do
    member do
      post 'rsvp', action: :rsvp
      post 'invite', action: :invite
    end

    collection do
      post 'events', action: :events
      post 'event', action: :create_event
    end
  end

  resources :forms, only: [:index] do
    resources :form_submissions, only: [:index, :create]
  end

  resources :teams, only: [:index, :show, :create, :update, :destroy] do
    put 'update_organization_image', action: :update_organization_image, on: :member
    resources :organization_user_roles, only: [:create, :destroy]
  end

  resources :meeting_notes, only: [:index, :show]

  resources :channels, only: [:index, :show, :create, :destroy] do
    resources :messages, only: [:create]
  end

  resources :direct_messages, only: [:index, :show, :create, :destroy] do
    resources :messages, only: [:create]
  end

  resources :dues, only: [:index]
  resources :dues_transactions, only: [:create]

  resources :budgets, only: [:index, :show] do
    resources :budget_transactions, only: [:create]
  end

  resources :organization_links, only: [:index, :create, :destroy]

  resources :birthdays, only: [:index]

  resources :approvals, only: [:index] do
    post 'approve', action: :approve, on: :member
  end

  get 'st_page/:page', to: 'static_pages#page'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

end
