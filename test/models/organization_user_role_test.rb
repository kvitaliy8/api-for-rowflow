require 'test_helper'

class OrganizationUserRoleTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:role)
		should belong_to(:organization)
		should belong_to(:user)
	end

end
