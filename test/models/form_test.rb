require 'test_helper'

class FormTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:organization)

		should have_many(:form_submissions).dependent(:destroy)
	end

	context "validations" do
		should validate_presence_of(:name)
		should validate_presence_of(:notes)
	end

end
