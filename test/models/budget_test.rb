require 'test_helper'

class BudgetTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:budgetable)

		should have_many(:budget_transactions).dependent(:destroy)
	end

	context "validations" do
		should validate_presence_of(:name)
	end

end
