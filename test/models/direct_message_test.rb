require 'test_helper'

class DirectMessageTest < ActiveSupport::TestCase

	context "associations" do
		should have_many(:message_groups_users).dependent(:destroy)
		should have_many(:users).through(:message_groups_users)
		should have_many(:messages).dependent(:destroy)
	end

end
