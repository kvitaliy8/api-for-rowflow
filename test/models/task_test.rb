require 'test_helper'

class TaskTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:assigned_to_user)
		should belong_to(:assigned_by_user)
	end

	context "validations" do
		should validate_presence_of(:notes)
	end

end
