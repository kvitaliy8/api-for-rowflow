require 'test_helper'

class FormSubmissionTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:user)
		should belong_to(:form)
	end

end
