require 'test_helper'

class MessageGroupsUserTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:user)
		should belong_to(:direct_message)
	end
end
