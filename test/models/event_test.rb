require 'test_helper'

class EventTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:organization)
		should belong_to(:created_by_user)
		should have_many(:event_invites).dependent(:destroy)
		should have_many(:users).through(:event_invites)
	end

	context "validations" do
		should validate_presence_of(:name)
		should validate_presence_of(:occurs_at)
	end

end
