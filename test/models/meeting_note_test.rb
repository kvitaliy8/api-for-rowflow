require 'test_helper'

class MeetingNoteTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:organization)
	end

	context "validations" do
		should validate_presence_of(:name)
		should validate_presence_of(:notes)
	end

end
