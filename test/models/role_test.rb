require 'test_helper'

class RoleTest < ActiveSupport::TestCase

	context "associations" do
		should have_many(:role_permissions)
		should have_many(:organization_user_roles)
		should have_many(:organizations).through(:organization_user_roles)
		should have_many(:users).through(:organization_user_roles)
	end

	context "validations" do
		should validate_presence_of(:title)
	end

end
