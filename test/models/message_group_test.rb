require 'test_helper'

class MessageGroupTest < ActiveSupport::TestCase

	context "associations" do
		should have_many(:messages).dependent(:destroy)
	end

end
