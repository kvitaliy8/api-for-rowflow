require 'test_helper'

class CommitteeTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:organizable)
		should have_many(:organization_user_roles).dependent(:destroy)
		should have_many(:roles).through(:organization_user_roles)
		should have_many(:users).through(:organization_user_roles)
		should have_many(:budgets).dependent(:destroy)
		should have_many(:forms).dependent(:destroy)
		should have_many(:events).dependent(:destroy)
		should have_many(:meeting_notes).dependent(:destroy)
		should have_many(:channels).dependent(:destroy)
	end

	context "validations" do
		should validate_presence_of(:name)
	end

end