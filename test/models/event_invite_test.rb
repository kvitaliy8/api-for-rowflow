require 'test_helper'

class EventInviteTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:event)
		should belong_to(:user)
	end

end
