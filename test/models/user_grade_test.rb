require 'test_helper'

class UserGradeTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:user)
	end

	context "validations" do
		should validate_presence_of(:course)
		should validate_presence_of(:grade)
	end

end
