require 'test_helper'

class ChannelTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:organization)
		should have_many(:messages).dependent(:destroy)
	end

	context "validations" do
		should validate_presence_of(:name)
	end
end
