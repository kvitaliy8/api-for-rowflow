require 'test_helper'

class RolePermissionTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:role)
	end

	context "validations" do
		should validate_presence_of(:allow)
		should validate_inclusion_of(:allow).in_array(RolePermission::PERMISSIONS)
	end

end
