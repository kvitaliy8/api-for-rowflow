require 'test_helper'

class MessageTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:from_user)
		should belong_to(:message_group)
	end

	context "validations" do
		should validate_presence_of(:message)
	end

end
