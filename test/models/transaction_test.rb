require 'test_helper'

class TransactionTest < ActiveSupport::TestCase

	context "associations" do
		should belong_to(:transactable)
		should belong_to(:created_by_user)
	end

	context "validations" do
		should validate_presence_of(:occurred_at)
		should validate_presence_of(:amount)
		should validate_numericality_of(:amount).is_greater_than_or_equal_to(0)
	end

end
