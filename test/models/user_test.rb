require 'test_helper'

class UserTest < ActiveSupport::TestCase

	context "associations" do
		should have_many(:organizations).through(:organization_user_roles)
		should have_many(:organization_user_roles).dependent(:destroy)

		should have_many(:roles).through(:organization_user_roles)
		should have_many(:role_permissions).through(:roles)

		should have_many(:meeting_notes).through(:organizations)
		should have_many(:channels).through(:organizations)

		should have_many(:forms).through(:organizations)
		should have_many(:form_submissions).dependent(:destroy)

		should have_many(:points_transactions).dependent(:destroy)

		should have_many(:dues).through(:organizations)
		should have_many(:dues_transactions)

		should have_many(:event_invites).dependent(:destroy)
		should have_many(:events).through(:event_invites)

		should have_many(:tasks).dependent(:destroy)
		should have_many(:created_tasks)

		should have_many(:messages)
		should have_many(:message_groups_users).dependent(:destroy)
		should have_many(:direct_messages).through(:message_groups_users)

		should have_many(:approvals)
	end

	context "validations" do
		should validate_presence_of(:first_name)
		should validate_presence_of(:last_name)
		should validate_presence_of(:date_of_birth)
		should validate_presence_of(:major)
		should validate_presence_of(:cellphone)
	end

end
