require 'test_helper'

class Api::DuesTransactionsControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_dues_transactions_path, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's dues transactions successfully" do
		get api_dues_transactions_path, headers: @auth_headers
		assert_response :success
	end

	test "CREATE fails authenticate_with_token!" do
		create_params = {occurred_at: DateTime.now, amount: 500, notes: "Annual Dues"}
		post api_dues_transactions_path, params: create_params, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "CREATE should return created dues transaction" do
		create_params = {occurred_at: DateTime.now, amount: 500, notes: "Annual Dues"}

		post api_dues_transactions_path, params: create_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['amount'], 500
	end

end
