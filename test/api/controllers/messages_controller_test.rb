require 'test_helper'

class Api::MessagesControllerTest < ActionDispatch::IntegrationTest

    def setup
        login_set_auth_headers
    end

    test "INDEX 'channel' fails authenticate_with_token!" do
        get api_channel_messages_path(channel_id: 1), headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "INDEX 'channel' should return message group's messages successfully" do
        get api_channel_messages_path(channel_id: 1), headers: @auth_headers
        assert_response :success
    end

    test "CREATE 'channel' fails authenticate_with_token!" do
        create_params = {message: "Oh hello..."}
        post api_channel_messages_path(channel_id: 1), params: create_params, headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "CREATE 'channel' should return created message" do
        create_params = {message: "Oh hello..."}

        post api_channel_messages_path(channel_id: 1), params: create_params, headers: @auth_headers
        assert_response :success
        assert_equal json_response['message_group_id'], 1
        assert_equal json_response['from_user_id'], 1
        assert_equal json_response['message'], "Oh hello..."
    end

    test "INDEX 'direct message' fails authenticate_with_token!" do
        get api_direct_message_messages_path(direct_message_id: 2), headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "INDEX 'direct message' should return message group's messages successfully" do
        get api_direct_message_messages_path(direct_message_id: 2), headers: @auth_headers
        assert_response :success
    end

    test "CREATE 'direct message' fails authenticate_with_token!" do
        create_params = {message: "Oh hello..."}
        post api_direct_message_messages_path(direct_message_id: 2), params: create_params, headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "CREATE 'direct message' should return created message" do
        create_params = {message: "Oh hello..."}

        post api_direct_message_messages_path(direct_message_id: 2), params: create_params, headers: @auth_headers
        assert_response :success
        assert_equal json_response['message_group_id'], 2
        assert_equal json_response['from_user_id'], 1
        assert_equal json_response['message'], "Oh hello..."
    end

end
