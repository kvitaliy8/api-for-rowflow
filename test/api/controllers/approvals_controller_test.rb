require 'test_helper'

class Api::ApprovalsControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_approvals_path, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's approvals successfully" do
		get api_approvals_path, headers: @auth_headers
		assert_response :success
	end

	test "APPROVE fails authenticate_with_token!" do
		post approve_api_approval_path(id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "APPROVE should set approval's approved_at" do
		post approve_api_approval_path(id: 1), headers: @auth_headers
		assert_response :success
		assert json_response['approved_at']
	end

end
