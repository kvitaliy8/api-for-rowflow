require 'test_helper'

class Api::SessionsControllerTest < ActionDispatch::IntegrationTest

	test "CREATE should login successfully" do
		login_headers = {
			'X-API-EMAIL': 'heyhey123@yahoo.com',
			'X-API-PASS': 'Password123'
		}

		post api_login_path, headers: login_headers

		assert_response :success
		assert_equal json_response["email"], 'heyhey123@yahoo.com'
		assert_equal json_response["first_name"], 'Chiquita'
		assert_equal json_response["last_name"], 'Juarez'
		assert json_response["auth_token"]
	end

	test "CREATE should fail login with invalid password" do
		login_headers = {
			'X-API-EMAIL': 'heyhey123@yahoo.com',
			'X-API-PASS': 'badpass'
		}

		post api_login_path, headers: login_headers
		assert_response :unauthorized
	end

	test "CREATE should fail login with invalid email" do
		login_headers = {
			'X-API-EMAIL': 'badEmail@yahoo.com',
			'X-API-PASS': 'Password123'
		}

		post api_login_path, headers: login_headers
		assert_response :unprocessable_entity
	end

	test "DESTROY should logout successfully" do
		# Log them in to get an auth_token
		login_headers = {
			'X-API-EMAIL': 'heyhey123@yahoo.com',
			'X-API-PASS': 'Password123'
		}
		post api_login_path, headers: login_headers
		assert_response :success
		auth_token = json_response["auth_token"]

		# Log them out using the auth_token
		logout_headers = {
			'Authorization': auth_token
		}
		post api_logout_path, headers: logout_headers
		assert_response :no_content
	end

	test "DESTROY should fail logout when user not found" do
		# Log them in to get an auth_token
		login_headers = {
			'X-API-EMAIL': 'heyhey123@yahoo.com',
			'X-API-PASS': 'Password123'
		}
		post api_login_path, headers: login_headers
		assert_response :success
		auth_token = json_response["auth_token"]

		# Log them out using the auth_token
		logout_headers = {
			'Authorization': 'badAuth'
		}
		post api_logout_path, headers: logout_headers
		assert_response :not_found
	end

end
