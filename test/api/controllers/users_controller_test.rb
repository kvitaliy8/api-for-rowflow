require 'test_helper'

class Api::UsersControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_users_path, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's directory successfully" do
		get api_users_path, headers: @auth_headers
		assert_response :success
	end

	test "SHOW fails authenticate_with_token!" do
		get api_user_path(id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "SHOW should return user's own info" do
		get api_user_path(id: 1), headers: @auth_headers
		assert_response :success
		assert_equal json_response['email'], 'heyhey123@yahoo.com'
	end

	test "SHOW should allow a user to see another user's info" do
		get api_user_path(id: 2), headers: @auth_headers
		assert_response :success
		assert_equal json_response['email'], 's3@yahoo.com'
	end

	test "UPDATE fails authenticate_with_token!" do
		put api_user_path(id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "UPDATE should return updated user's own info" do
		update_params = {'first_name': 'Riley', 'image': fixture_file_upload('/files/cocacola.png', 'image/png'), 'cellphone': '8433435510',
						'facebook_url': 'https://www.facebook.com/yoohoo', 'instagram_url': 'https://www.instagram.com/lol1', 'major': 'Computer Science',
						'clubs_orgs': 'CofC Volunteers', 'date_of_birth': '1994-03-28', 'hometown': 'Summerville', 'schedule_url': 'https://www.college.com',
						'previous_semester_gpa': 3.0, 'cumulative_gpa': 3.5, 'emergency_contact_name': 'Sherri', 'year_in_school': 'Senior', 'emergency_contact_number': '8438675309'}

		put api_user_path(id: 1), params: update_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['first_name'], 'Riley'
	end

	test "UPDATE should block a user with insufficient perms from updating another user's info" do
		update_params = {'first_name': 'Riley', 'image': fixture_file_upload('/files/cocacola.png', 'image/png'), 'cellphone': '8433435510',
						'facebook_url': 'https://www.facebook.com/yoohoo', 'instagram_url': 'https://www.instagram.com/lol1', 'major': 'Computer Science',
						'clubs_orgs': 'CofC Volunteers', 'date_of_birth': '1994-03-28', 'hometown': 'Summerville', 'schedule_url': 'https://www.college.com',
						'previous_semester_gpa': 3.0, 'cumulative_gpa': 3.5, 'emergency_contact_name': 'Sherri', 'year_in_school': 'Senior', 'emergency_contact_number': '8438675309'}

		put api_user_path(id: 2), params: update_params, headers: @auth_headers
		assert_response :forbidden
	end

	test "UPDATE should allow a user with perms to update another user's info" do
		add_user_permissions 'heyhey123@yahoo.com', ['create_user']

		update_params = {'first_name': 'Riley', 'image': fixture_file_upload('/files/cocacola.png', 'image/png'), 'cellphone': '8433435510',
						'facebook_url': 'https://www.facebook.com/yoohoo', 'instagram_url': 'https://www.instagram.com/lol1', 'major': 'Computer Science',
						'clubs_orgs': 'CofC Volunteers', 'date_of_birth': '1994-03-28', 'hometown': 'Summerville', 'schedule_url': 'https://www.college.com',
						'previous_semester_gpa': 3.0, 'cumulative_gpa': 3.5, 'emergency_contact_name': 'Sherri', 'year_in_school': 'Senior', 'emergency_contact_number': '8438675309'}

		put api_user_path(id: 2), params: update_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['first_name'], 'Riley'

		clean_up_extra_permissions
	end

end
