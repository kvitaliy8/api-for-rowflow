require 'test_helper'

class Api::BudgetTransactionsControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_budget_budget_transactions_path(budget_id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's points successfully" do
		get api_budget_budget_transactions_path(budget_id: 1), headers: @auth_headers
		assert_response :success
	end

	test "CREATE fails authenticate_with_token!" do
		create_params = {occurred_at: DateTime.now, amount: 500, notes: "Doing my Homework", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}
		post api_budget_budget_transactions_path(budget_id: 1), params: create_params, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "CREATE should return created points" do
		create_params = {occurred_at: DateTime.now, amount: 100, notes: "Doing my Homework", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}

		post api_budget_budget_transactions_path(budget_id: 1), params: create_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['amount'], 100
	end

end
