require 'test_helper'

class Api::PointsTransactionsControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_user_points_transactions_path(user_id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's points successfully" do
		get api_user_points_transactions_path(user_id: 1), headers: @auth_headers
		assert_response :success
	end

	test "INDEX should block a user with insufficient perms from seeing another user's points" do
		get api_user_points_transactions_path(user_id: 2), headers: @auth_headers
		assert_response :forbidden
	end

	test "INDEX should allow a user with perms to see another user's points" do
		add_user_permissions 'heyhey123@yahoo.com', ['view_points_transaction']

		get api_user_points_transactions_path(user_id: 2), headers: @auth_headers
		assert_response :success

		clean_up_extra_permissions
	end

	test "CREATE fails authenticate_with_token!" do
		create_params = {occurred_at: DateTime.now, amount: 500, notes: "Doing my Homework"}
		post api_user_points_transactions_path(user_id: 1), params: create_params, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "CREATE should return created points" do
		create_params = {occurred_at: DateTime.now, amount: 500, notes: "Doing my Homework"}

		post api_user_points_transactions_path(user_id: 1), params: create_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['amount'], 500
	end

	test "CREATE should block a user with insufficient perms from creating points for another user" do
		create_params = {occurred_at: DateTime.now, amount: 500, notes: "Doing my Homework"}

		post api_user_points_transactions_path(user_id: 2), params: create_params, headers: @auth_headers
		assert_response :forbidden
	end

	test "CREATE should allow a user with perms to create points for another user" do
		add_user_permissions 'heyhey123@yahoo.com', ['create_points_transaction']

		create_params = {occurred_at: DateTime.now, amount: 500, notes: "Doing my Homework"}

		post api_user_points_transactions_path(user_id: 2), params: create_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['amount'], 500

		clean_up_extra_permissions
	end

end
