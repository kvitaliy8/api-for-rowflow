require 'test_helper'

class Api::DuesControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_dues_path, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's approvals successfully" do
		get api_dues_path, headers: @auth_headers
		assert_response :success
	end

end