require 'test_helper'

class Api::DirectMessagesControllerTest < ActionDispatch::IntegrationTest

    def setup
        login_set_auth_headers
    end

    test "INDEX fails authenticate_with_token!" do
        get api_direct_messages_path, headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "INDEX should return user's Direct Messages successfully" do
        get api_direct_messages_path, headers: @auth_headers
        assert_response :success
    end


    test "CREATE fails authenticate_with_token!" do
        create_params = {name: "Hot Political Discussion"}
        post api_direct_messages_path, params: create_params, headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "CREATE should block a user with insufficient perms from creating a Direct Message" do
        create_params = {name: "Hot Political Discussion"}

        post api_direct_messages_path, params: create_params, headers: @auth_headers
        assert_response :forbidden
    end

    test "CREATE should allow a user with perms to create a Direct Message" do
        add_user_permissions 'heyhey123@yahoo.com', ['create_direct_message']

        create_params = {name: "Hot Political Discussion"}

        post api_direct_messages_path, params: create_params, headers: @auth_headers
        assert_response :success
        assert_equal json_response['name'], "Hot Political Discussion"

        # Make sure the user got added to the message group
        user = User.find_by_email('heyhey123@yahoo.com')
        assert user.direct_messages.map(&:name).include? "Hot Political Discussion"

        clean_up_extra_permissions
    end

end
