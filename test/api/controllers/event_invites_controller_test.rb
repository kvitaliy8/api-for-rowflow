require 'test_helper'

class Api::EventInvitesControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_event_invites_path, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's event invites successfully" do
		get api_event_invites_path, headers: @auth_headers
		assert_response :success
	end

	test "RSVP fails authenticate_with_token!" do
		post rsvp_api_event_invite_path(id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "RSVP should set event invite's rsvp_at" do
		post rsvp_api_event_invite_path(id: 1), headers: @auth_headers
		assert_response :success
		assert json_response['rsvp_at']
	end

	test "CHECK_IN fails authenticate_with_token!" do
		post check_in_api_event_invite_path(id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "CHECK_IN should not set event invite's checked_in_at without lat/long params" do
		post check_in_api_event_invite_path(id: 1), headers: @auth_headers
		assert_response :unprocessable_entity
	end


	test "CHECK_IN should set event invite's checked_in_at" do
		check_in_params = {location_lat: 123.44, location_long: -43.22}
		post check_in_api_event_invite_path(id: 1), params: check_in_params, headers: @auth_headers
		assert_response :success
		assert json_response['checked_in_at']
	end

end
