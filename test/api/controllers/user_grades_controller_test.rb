require 'test_helper'

class Api::UserGradesControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_user_grades_path, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's grades successfully" do
		get api_user_grades_path, headers: @auth_headers
		assert_response :success
	end

	test "SHOW fails authenticate_with_token!" do
		get api_user_grade_path(id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "SHOW should return user's grade" do
		get api_user_grade_path(id: 1), headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], 'Sorry this is late!'
	end

	test "CREATE fails authenticate_with_token!" do
		create_params = {course: "Ecology Final", notes: "I did it!", grade: "A", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}

		post api_user_grades_path, params: create_params, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "CREATE should return created user grade" do

		create_params = {course: "Ecology Final", notes: "I did it!", grade: "A", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}

		post api_user_grades_path, params: create_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], "I did it!"
	end

end
