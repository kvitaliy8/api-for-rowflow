require 'test_helper'

class Api::TasksControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_user_tasks_path(user_id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's tasks successfully" do
		get api_user_tasks_path(user_id: 1), headers: @auth_headers
		assert_response :success
	end

	test "INDEX should block a user with insufficient perms from seeing another user's tasks" do
		get api_user_tasks_path(user_id: 2), headers: @auth_headers
		assert_response :forbidden
	end

	test "INDEX should allow a user with perms to see another user's tasks" do
		user = User.find_by_email('heyhey123@yahoo.com')
		permission = user.roles.first.role_permissions.create(allow: 'view_task')

		get api_user_tasks_path(user_id: 2), headers: @auth_headers
		assert_response :success

		permission.destroy  #cleanup
	end

	test "SHOW fails authenticate_with_token!" do
		get api_user_task_path(user_id: 1, id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "SHOW should return user's own task" do
		get api_user_task_path(user_id: 1, id: 1), headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], 'You need to do your homework.'
	end

	test "SHOW should block a user with insufficient perms from seeing another user's task" do
		get api_user_task_path(user_id: 2, id: 2), headers: @auth_headers
		assert_response :forbidden
	end

	test "SHOW should allow a user with perms to see another user's task" do
		add_user_permissions 'heyhey123@yahoo.com', ['view_task']

		get api_user_task_path(user_id: 2, id: 2), headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], 'You have been slack.'

		clean_up_extra_permissions
	end

	test "CREATE fails authenticate_with_token!" do
		create_params = {due_at: DateTime.now, notes: "Just get it done", task_type: "Pay Up"}
		post api_user_tasks_path(user_id: 1), params: create_params, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "CREATE should return created task" do

		create_params = {due_at: DateTime.now, notes: "Just get it done", task_type: "Pay Up", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}

		post api_user_tasks_path(user_id: 1), params: create_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], "Just get it done"
	end

	test "CREATE should block a user with insufficient perms from creating a task for another user" do
		create_params = {due_at: DateTime.now, notes: "Just get it done", task_type: "Pay Up", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}

		post api_user_tasks_path(user_id: 2), params: create_params, headers: @auth_headers
		assert_response :forbidden
	end

	test "CREATE should allow a user with perms to create a task for another user" do
		add_user_permissions 'heyhey123@yahoo.com', ['create_task']

		create_params = {due_at: DateTime.now, notes: "Just get it done", task_type: "Pay Up", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}
		post api_user_tasks_path(user_id: 2), params: create_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], "Just get it done"

		clean_up_extra_permissions
	end

	test "UPDATE fails authenticate_with_token!" do
		put api_user_task_path(user_id: 1, id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "UPDATE should return updated user's task" do
		update_params = {due_at: DateTime.now, notes: "Just get it done", task_type: "Pay Up", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}

		put api_user_task_path(user_id: 1, id: 1), params: update_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], "Just get it done"
	end

	test "UPDATE should block a user with insufficient perms from updating another user's task" do
		update_params = {due_at: DateTime.now, notes: "Just get it done", task_type: "Pay Up", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}

		put api_user_task_path(user_id: 2, id: 2), params: update_params, headers: @auth_headers
		assert_response :forbidden
	end

	test "UPDATE should allow a user with perms to update another user's task" do
		add_user_permissions 'heyhey123@yahoo.com', ['create_task']

		update_params = {due_at: DateTime.now, notes: "Just get it done", task_type: "Pay Up", attachment: fixture_file_upload('/files/cocacola.png', 'image/png')}
		put api_user_task_path(user_id: 2, id: 2), params: update_params, headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], "Just get it done"

		clean_up_extra_permissions
	end

	test "COMPLETE fails authenticate_with_token!" do
		post complete_api_user_task_path(user_id: 1, id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "COMPLETE should return completed task" do
		post complete_api_user_task_path(user_id: 1, id: 1), headers: @auth_headers
		assert_response :success
		assert json_response['completed_at']
	end

	test "COMPLETE should block a user with insufficient perms from completing another user's task" do
		post complete_api_user_task_path(user_id: 2, id: 2), headers: @auth_headers
		assert_response :forbidden
	end

	test "COMPLETE should allow a user with perms to complete another user's task" do
		add_user_permissions 'heyhey123@yahoo.com', ['create_task']

		post complete_api_user_task_path(user_id: 2, id: 2), headers: @auth_headers
		assert_response :success
		assert json_response['completed_at']

		clean_up_extra_permissions
	end

	test "INCOMPLETE fails authenticate_with_token!" do
		post incomplete_api_user_task_path(user_id: 1, id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INCOMPLETE should return completed task" do
		post incomplete_api_user_task_path(user_id: 1, id: 1), headers: @auth_headers
		assert_response :success
		assert_nil json_response['completed_at']
	end

	test "INCOMPLETE should block a user with insufficient perms from completing another user's task" do
		post incomplete_api_user_task_path(user_id: 2, id: 2), headers: @auth_headers
		assert_response :forbidden
	end

	test "INCOMPLETE should allow a user with perms to complete another user's task" do
		add_user_permissions 'heyhey123@yahoo.com', ['create_task']

		post incomplete_api_user_task_path(user_id: 2, id: 2), headers: @auth_headers
		assert_response :success
		assert_nil json_response['completed_at']

		clean_up_extra_permissions
	end

end
