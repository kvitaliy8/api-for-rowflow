require 'test_helper'

class Api::MessageGroupsUsersControllerTest < ActionDispatch::IntegrationTest

    def setup
        login_set_auth_headers
    end

    test "INDEX fails authenticate_with_token!" do
        get api_direct_message_message_groups_users_path(direct_message_id: 2), headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "INDEX should return user's message groups successfully" do
        get api_direct_message_message_groups_users_path(direct_message_id: 2), headers: @auth_headers
        assert_response :success
    end


    test "CREATE fails authenticate_with_token!" do
        create_params = {user_id: 1}
        post api_direct_message_message_groups_users_path(direct_message_id: 2), params: create_params, headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "CREATE should block a user with insufficient perms from creating a message group" do
        create_params = {user_id: 1}

        post api_direct_message_message_groups_users_path(direct_message_id: 2), params: create_params, headers: @auth_headers
        assert_response :forbidden
    end

    test "CREATE should allow a user with perms to create a message group" do
        add_user_permissions 'heyhey123@yahoo.com', ['create_message_groups_user']

        create_params = {user_id: 1}

        post api_direct_message_message_groups_users_path(direct_message_id: 2), params: create_params, headers: @auth_headers
        assert_response :success
        assert_equal json_response['message_group_id'], 2
        assert_equal json_response['user_id'], 1

        clean_up_extra_permissions
    end

    test "UPDATE fails authenticate_with_token!" do
        put api_direct_message_message_groups_user_path(direct_message_id: 2, id: 1), headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "UPDATE should return updated message_groups_user" do
        update_params = {'has_unread_messages': true}

        put api_direct_message_message_groups_user_path(direct_message_id: 2, id: 1), params: update_params, headers: @auth_headers
        assert_response :success
        assert_equal json_response['has_unread_messages'], true
    end

end
