require 'test_helper'

class Api::MeetingNotesControllerTest < ActionDispatch::IntegrationTest

	def setup
		login_set_auth_headers
	end

	test "INDEX fails authenticate_with_token!" do
		get api_meeting_notes_path, headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "INDEX should return user's meeting notes successfully" do
		get api_meeting_notes_path, headers: @auth_headers
		assert_response :success
	end

	test "SHOW fails authenticate_with_token!" do
		get api_meeting_note_path(id: 1), headers: {'Authorization': 'badAuth'}
		assert_response :unauthorized
	end

	test "SHOW should return user's meeting note" do
		get api_meeting_note_path(id: 1), headers: @auth_headers
		assert_response :success
		assert_equal json_response['notes'], 'Things went very well today'
	end

    test "CREATE fails authenticate_with_token!" do
        create_params = {organization_id: 1, notes: "Great Meeting!", name: "Monthly"}
        post api_meeting_notes_path, params: create_params, headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "CREATE should return created meeting note" do
        create_params = {organization_id: 1, notes: "Great Meeting!", name: "Monthly", occurred_at: "Sun Mar 28 1993 13:30:00 GMT+0500 (CDT)"}

        post api_meeting_notes_path, params: create_params, headers: @auth_headers
        assert_response :success
    end

    test "CREATE should not allow user to create meeting notes for an org they're not related to" do
        create_params = {organization_id: 3, notes: "Great Meeting!", name: "Monthly"}

        post api_meeting_notes_path, params: create_params, headers: @auth_headers
        assert_response :unprocessable_entity
    end

end
