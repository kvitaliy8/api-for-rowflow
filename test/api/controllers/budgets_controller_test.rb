require 'test_helper'

class Api::BudgetsControllerTest < ActionDispatch::IntegrationTest

    def setup
        login_set_auth_headers
    end

    test "INDEX fails authenticate_with_token!" do
        get api_budgets_path, headers: {'Authorization': 'badAuth'}
        assert_response :unauthorized
    end

    test "INDEX should return user's channels successfully" do
        get api_budgets_path, headers: @auth_headers
        assert_response :success
    end

end
