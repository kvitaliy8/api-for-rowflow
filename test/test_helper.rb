require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
	fixtures :all

	def json_response
		ActiveSupport::JSON.decode @response.body
	end

	def login_set_auth_headers
		# Log them in to get an auth_token
		login_headers = {
			'X-API-EMAIL': 'heyhey123@yahoo.com',
			'X-API-PASS': 'Password123'
		}
		post api_login_path, headers: login_headers
		@auth_headers = {
			'Authorization': json_response["auth_token"]
		}
	end


	def add_user_permissions email, permissions
		@created_permissions = []

		user = User.find_by_email('heyhey123@yahoo.com')
		raise 'User not found' unless user.present?

		userRole = user.roles.first
		raise 'No User Roles found' unless userRole.present?

		permissions.each do |p|
			@created_permissions.push userRole.role_permissions.create(allow: p)
		end
	end

	def clean_up_extra_permissions
		@created_permissions.each do |p|
			p.destroy
		end
	end

end
