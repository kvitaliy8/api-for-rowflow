RailsAdmin.config_model Channel do

  list do
    field :name
    field :organization
    field :created_at
    field :updated_at
  end

  edit do
    field :name
    field :organization
  end

end