RailsAdmin.config_model DirectMessage do

  list do
    field :id
    field :name
    field :type
    field :created_at
    field :updated_at
  end

  # edit do
  #   field :name
  #   field :organization
  # end

end