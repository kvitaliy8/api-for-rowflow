RailsAdmin.config_model User do

  list do
    field :id
    field :email
    field :first_name
    field :last_name
    field :major
    field :hometown

    # date_of_birth: nil,
    # cellphone: nil,
    # hometown: nil,
    # schedule_url: nil,
    # previous_semester_gpa: nil,
    # cumulative_gpa: nil,
    # emergency_contact_name: nil,
    # emergency_contact_number: nil,
    # status: nil,
    # image_file_name: nil,
    # image_content_type: nil,
    # image_file_size: nil,
    # image_updated_at: nil,
    # clubs_orgs: nil,
    # year_in_school: nil

    # configure :email do
    #   searchable true
    # end

    filters [:email]
  end

  # edit do
  # end

  # show do
  # end

end