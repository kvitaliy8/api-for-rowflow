class FormSubmission < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:user
	belongs_to	:form

	has_many 	:approvals,			as: :approvable, dependent: :destroy

	has_attached_file :attachment
	do_not_validate_attachment_file_type :attachment

	after_create :generate_necessary_approvals

	def generate_necessary_approvals
		form.approvers.each do |approver|
			approvals.create(approver: approver)
		end
	end

end
