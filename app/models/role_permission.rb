class RolePermission < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:role

	# VALIDATIONS ----------------------------------------------------------
	validates :allow, presence: true

	PERMISSIONS = DEFAULT_ROLES["permissions"]
	validates_inclusion_of :allow, in: PERMISSIONS

end
