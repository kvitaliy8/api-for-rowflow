class Role < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	has_many	:role_permissions, 			dependent: :destroy
	has_many	:organization_user_roles
	has_many	:organizations,				through: :organization_user_roles
	has_many	:users,						through: :organization_user_roles

	# VALIDATIONS ----------------------------------------------------------
	validates :title, presence: true


	def add_permissions perms
		unless perms.instance_of? Array and perms.all? {|p| p.instance_of? String }
			errors.add(:permissions, "must be an Array of Strings")
			return false
		end

		current_permissions = self.permissions
		perms.each do |p|
			if RolePermission::PERMISSIONS.include? p
				unless current_permissions.include? p
					role_permissions.create(allow: p)
				end
			else
				errors.add(:permissions, "Invalid Permission: #{p}")
				return false
			end
		end

		return true
	end

	def remove_permissions perms
		unless perms.instance_of? Array and perms.all? {|p| p.instance_of? String }
			errors.add(:permissions, "must be an Array of Strings")
			return false
		end

		current_permissions = self.permissions
		perms.each do |p|
			if current_permissions.include? p
				role_permissions.where(allow: p).destroy_all
			else
				errors.add(:permissions, "Invalid Permission: #{p}")
				return false
			end
		end

		return true
	end

	def permissions
		role_permissions.map(&:allow)
	end

	# CLASS METHODS

	def self.update_defaults
		# Update the default set of roles to the db that are defined in config/default_roles.yml
		# Run after Rails start

		DEFAULT_ROLES["roles"].each do |key, role_config|
			desired_permissions = [role_config["permissions"]].flatten.compact
			existing_role = self.find_by_default_name(key)
			unless existing_role.present?
				new_role = self.create(title: role_config["title"], description: role_config["description"], default_name: key)
				new_role.add_permissions desired_permissions
			else
				existing_role.update(title: role_config["title"], description: role_config["description"])
				existing_role.add_permissions desired_permissions - existing_role.permissions
				existing_role.remove_permissions existing_role.permissions - desired_permissions
			end
		end
	end
end
