class Form < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to 	:organization
	has_many	:form_submissions,	dependent: :destroy

	# VALIDATIONS ----------------------------------------------------------
	validates :name, :notes, presence: true

	has_attached_file :attachment
	do_not_validate_attachment_file_type :attachment

	def approvers
		approver_roles_id.map do |role_id|
			user_roles = organization.organization_user_roles.where(role_id: role_id)
			if user_roles.any?
				user_roles.first.user
			else
				nil
			end
		end
	end

end
