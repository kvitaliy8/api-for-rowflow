class Due < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to 	:organization
	has_many	:dues_transactions, through: :organization

	# VALIDATIONS ----------------------------------------------------------
	validates :amount, :description, presence: true
	validates :amount, numericality: { greater_than_or_equal_to: 0 }

end
