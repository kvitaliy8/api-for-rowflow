class Committee < Organization

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:organizable,	polymorphic: true

	def all_users
		associated_users = []
		associated_users << users

		associated_users.flatten.uniq
	end
end