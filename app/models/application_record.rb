class ApplicationRecord < ActiveRecord::Base
  	self.abstract_class = true

  	def approved?
		if approvals.any?
			approvals.map(&:approved_at).all?
		else
			nil
		end
	end

	def self.create_permission_name
		"create_#{self.name.underscore}"
	end

	def self.view_permission_name
		"view_#{self.name.underscore}"
	end
end
