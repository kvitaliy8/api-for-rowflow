class Event < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to 	:organization
	belongs_to	:created_by_user,		class_name: "User"

	has_many	:event_invites, 		dependent: :destroy
	has_many	:users,					through: :event_invites
	has_many 	:approvals,				as: :approvable, dependent: :destroy
	has_one		:budget,				as: :budgetable, dependent: :destroy

	# VALIDATIONS ----------------------------------------------------------
	validates :name, :occurs_at, presence: true

	has_attached_file :attachment
	do_not_validate_attachment_file_type :attachment

	def month
		# Month (June 2017)
		occurs_at.strftime("%B %Y")
	end

	def date
		# Date (Monday June 7, 2017)
		occurs_at.strftime("%A %B %-d, %Y")
	end

	def time
		# Time (8 PM)
		occurs_at.strftime("%-I %p")
	end

	def venue_city_state_zip
		"#{venue_city}, #{venue_state} #{venue_zipcode}"
	end

	def rsvp? user
		event_invites.where(user_id: user.id, rsvp_at: nil).present? ? false : true
	end

end
