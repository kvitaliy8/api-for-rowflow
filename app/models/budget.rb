class Budget < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to 	:budgetable, 			polymorphic: true

	has_many 	:budget_transactions,	as: :transactable, dependent: :destroy
	has_many 	:approvals,				as: :approvable, dependent: :destroy

	# VALIDATIONS ----------------------------------------------------------
	validates :name, presence: true

	def subtract_from_remaining! amt
		self.remaining -= amt
		self.save
	end

	def total_spent
		budget_transactions.sum(:amount)
	end

end
