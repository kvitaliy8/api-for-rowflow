class Chapter < Organization

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:organizable,	polymorphic: true

	has_many :committees,		as: :organizable, dependent: :destroy

	def all_users
		# All users associated to this chapter and any of its committees
		associated_users = []
		associated_users << users
		associated_users << committees.map(&:all_users)

		associated_users.flatten.uniq
	end


end