class OrganizationUserRole < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:user
	belongs_to	:organization
	belongs_to	:role

end
