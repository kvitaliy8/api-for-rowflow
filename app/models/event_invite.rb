class EventInvite < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to :event
	belongs_to :user


	def check_in current_lat, current_long
		event_lat, event_long = event.location_lat, event.location_long
		unless (event_lat.present? and event_long.present?)
			self.update(checked_in_at: DateTime.now)
			return true
		end

		unless (current_lat.present? and current_long.present?)
			errors.add(:coordinates, "location_lat and location_long are required to check in.")
			return false
		end

		# TODO GEOKIT
		self.update(checked_in_at: DateTime.now)
		return true
	end

end
