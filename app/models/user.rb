class User < ApplicationRecord

	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

	# ASSOCIATIONS ----------------------------------------------------------
	has_many	:organization_user_roles,	dependent: :destroy
	has_many	:organizations,				through: :organization_user_roles

	has_many	:roles,						through: :organization_user_roles
	has_many	:role_permissions,			through: :roles

	has_many	:expenses,					foreign_key: "created_by_user_id", class_name: "BudgetTransaction"

	has_many	:meeting_notes,				through: :organizations
	has_many 	:channels, 					through: :organizations

	has_many	:forms,						through: :organizations
	has_many	:form_submissions, 			dependent: :destroy

	has_many 	:points_transactions,		as: :transactable, dependent: :destroy

	has_many	:dues,						through: :organizations
	has_many	:dues_transactions,			as: :transactable

	has_many	:event_invites, 			dependent: :destroy
	has_many	:events,					through: :event_invites

	has_many	:tasks,						foreign_key: "assigned_to_user_id", dependent: :destroy
	has_many	:created_tasks,				foreign_key: "assigned_by_user_id", class_name: "Task"

	has_many	:messages,					foreign_key: "from_user_id"
	has_many	:message_groups_users, 		dependent: :destroy
	has_many 	:direct_messages, 			through: :message_groups_users

	has_many	:approvals,					foreign_key: "approver_id"

	has_many	:organization_links, 		through: :organizations

	has_many	:user_grades

	has_attached_file :image

	# VALIDATIONS ----------------------------------------------------------
	validates :first_name, :last_name, :date_of_birth, :major, :cellphone, presence: true
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

	before_create :generate_auth_token

	def generate_auth_token
		loop do
			self.auth_token = Devise.friendly_token
			break self.auth_token unless User.where(auth_token: self.auth_token).first
		end
	end

	###### CONVENIENCE METHODS ########

	def full_name= name
		split = name.split(' ', 2)
		self.first_name = split.first
		self.last_name = split.last
	end

	def full_name
		[first_name, last_name].join(' ')
	end

	def birthday
		date_of_birth.strftime("%B %-d")
	end

	def points
		points_transactions.where.not(id: nil).sum(&:amount)
	end

	def total_dues
		dues.where.not(id: nil).uniq.sum(&:amount)
	end

	def dues_paid
		dues_transactions.where.not(id: nil).uniq.sum(&:amount)
	end

	def dues_remaining
		total_dues - dues_paid
	end

	def directory
		organizations.map(&:all_users).flatten.uniq
	end

	def my_directory
		directory.reject{|u| u.id == id}
	end

	def unread_messages_count
		message_groups_users.where(has_unread_messages: true).size
	end

	def budgets
		# Get all budgets the user is associated to via events or organizations
		associated_budgets = []
		associated_budgets << events.map(&:budget)
		associated_budgets << organizations.map(&:budgets)

		associated_budgets.flatten.uniq.compact
	end

	def upcoming_events
		events.where("events.occurs_at >= ?", DateTime.now)
	end

	def past_events
		events.where("events.occurs_at < ?", DateTime.now)
	end

	def membership_engagement_score
		# Percentage of available past events that the user checked into
		potential_event_ids = past_events.map(&:id).compact
		checked_in_events = event_invites.where(event_id: potential_event_ids).where.not(checked_in_at: nil)

		(checked_in_events.length / (potential_event_ids.length > 0 ? potential_event_ids.length : 1)) * 100
	end


	################## PERMISSIONS #####################

	def permissions
		role_permissions.map(&:allow)
	end

	def has_permission? permission_name
		# Does this user have the permission?
		# returns: Boolean

		permissions.include? permission_name
	end

	def can_view? model
		has_permission? model.view_permission_name
	end

	def can_create? model
		has_permission? model.create_permission_name
	end

	def can_edit? model
		has_permission? model.create_permission_name
	end

	def can_delete? model
		has_permission? model.create_permission_name
	end

	def admin?
		has_permission? 'admin'
	end
end
