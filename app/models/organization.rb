class Organization < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------

	has_many	:organization_user_roles,	dependent: :destroy
	has_many	:roles,						through: :organization_user_roles
	has_many	:users,						through: :organization_user_roles

	has_many	:budgets,					as: :budgetable, dependent: :destroy
	has_many	:forms, 					dependent: :destroy
	has_many	:events, 					dependent: :destroy
	has_many	:meeting_notes, 			dependent: :destroy
	has_many	:dues, 						dependent: :destroy
	has_many	:dues_transactions,			through: :users
	has_many	:channels,					dependent: :destroy
	has_many	:organization_links,		dependent: :destroy

	has_attached_file :image

	# VALIDATIONS ----------------------------------------------------------
	validates :name, presence: true
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

end
