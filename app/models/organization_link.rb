class OrganizationLink < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to :organization

	# VALIDATIONS ----------------------------------------------------------
	validates :name, :link, presence: true

end
