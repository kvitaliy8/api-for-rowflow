class Transaction < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:transactable,		polymorphic: true
	belongs_to	:created_by_user,	class_name: "User"

	has_many 	:approvals,			as: :approvable, dependent: :destroy

	# VALIDATIONS ----------------------------------------------------------
	validates :occurred_at, :amount, :created_by_user_id, presence: true
	validates :amount, numericality: { greater_than_or_equal_to: 0 }

	has_attached_file :attachment
	do_not_validate_attachment_file_type :attachment
end
