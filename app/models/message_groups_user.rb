class MessageGroupsUser < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to :user
	belongs_to :direct_message,	foreign_key: "message_group_id"

end
