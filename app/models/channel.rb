class Channel < MessageGroup

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to :organization

	# VALIDATIONS ----------------------------------------------------------
	validates :name, presence: true

end
