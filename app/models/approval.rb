class Approval < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:approvable,		polymorphic: true
	belongs_to	:approver,			class_name: "User"

end
