class MeetingNote < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:organization

	# VALIDATIONS ----------------------------------------------------------
	validates :name, :notes, :occurred_at, presence: true

	has_attached_file :attachment
	do_not_validate_attachment_file_type :attachment
end
