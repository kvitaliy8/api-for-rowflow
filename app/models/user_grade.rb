class UserGrade < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to :user

	# VALIDATIONS ----------------------------------------------------------
	validates :course, :grade, presence: true

	has_attached_file :attachment
	do_not_validate_attachment_file_type :attachment

end
