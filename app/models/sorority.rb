class Sorority < Organization

	# ASSOCIATIONS ----------------------------------------------------------
	has_many :chapters,		as: :organizable, dependent: :destroy

	def all_users
		# All users associated to this sorority and any of its chapters
		associated_users = []
		associated_users << users
		associated_users << chapters.map(&:all_users)

		associated_users.flatten.uniq
	end

end