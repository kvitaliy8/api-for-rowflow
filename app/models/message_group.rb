class MessageGroup < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	has_many 	:messages,				dependent: :destroy

end
