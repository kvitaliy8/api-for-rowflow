class DirectMessage < MessageGroup

	# ASSOCIATIONS ----------------------------------------------------------
	has_many	:message_groups_users,	foreign_key: "message_group_id", dependent: :destroy
	has_many 	:users, 				through: :message_groups_users

	def message_groups_users_attributes= attributes
		self.message_groups_users ||= []
		attributes.each do |i, mgu_params|
			mgu = MessageGroupsUser.new mgu_params
			mgu.direct_message = self
			self.message_groups_users << mgu
		end
	end

	def add_user user
		self.message_groups_users ||= []
		self.message_groups_users << MessageGroupsUser.new(user: user, direct_message: self) unless self.message_groups_users.map(&:user_id).include? user.id
	end

	def add_user! user
		self.add_user user
		self.save
	end

	def users_except unwanted_user
		users.where.not(id: unwanted_user.id)
	end

	def display_users_except unwanted_user
		users_except(unwanted_user).map(&:full_name).join(', ')
	end

	def user_has_unread_messages? user
		mgu = self.message_groups_users.find_by_user_id user.id

		return mgu.has_unread_messages if mgu.present?

		return false
	end

	def users_now_have_unread_messages_except unwanted_user
		desired_mgus = self.message_groups_users.where.not(user_id: unwanted_user.id)
		desired_mgus.update_all(has_unread_messages: true)
	end

	def user_has_viewed_message_group user
		mgu = self.message_groups_users.find_by_user_id user.id
		mgu.update(has_unread_messages: false)  if mgu.present?
	end

	def self.find_or_create_by_users user_ids
		# Find or create a DirectMessage Group by the users who are in it.

		# If we find a DirectMessage with just these users, return it
		dms_with_these_users = DirectMessage.joins(:users).where(users: {id: user_ids}).uniq
		if dms_with_these_users.length
			dms_with_these_users.each do |dm|
				if (dm.message_groups_user_ids - user_ids) == []
					return dm
				end
			end
		end

		# Not found, so create it
		dm = DirectMessage.create()
		user_ids.each do |user_id|
			dm.message_groups_users.create(user_id: user_id)
		end

		return dm
	end
end
