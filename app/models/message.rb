class Message < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to	:from_user,		class_name: "User"
	belongs_to	:message_group

	# VALIDATIONS ----------------------------------------------------------
	validates :message, presence: true

end
