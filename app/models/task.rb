class Task < ApplicationRecord

	# ASSOCIATIONS ----------------------------------------------------------
	belongs_to 	:assigned_to_user,		class_name: "User"
	belongs_to	:assigned_by_user,		class_name: "User"

	has_many 	:approvals,				as: :approvable, dependent: :destroy

	# VALIDATIONS ----------------------------------------------------------
	validates :notes, :due_at, presence: true

	has_attached_file :attachment
	do_not_validate_attachment_file_type :attachment

	def owner_full_name
		self.assigned_by_user.full_name
	end

	def assigned_to_full_name
		self.assigned_to_user.full_name
  end

  def completed?
    self.completed_at.present?
  end

end
