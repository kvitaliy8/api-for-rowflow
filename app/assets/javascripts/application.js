// LIBS
//= require rails-ujs
//= require turbolinks
//= require jquery
//= require moment.min
//= require fullcalendar

// APP FILES
//= require turbolinks_events
//= require calendar
//= require menu
//= require profile
//= require modal
//= require tasks