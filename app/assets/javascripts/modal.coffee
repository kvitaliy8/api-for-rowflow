$(document).on 'ready', ->

  $(document).on 'click', '[data-modal-id]', ->
    modalId = $(this).data('modalId')
    $('#' + modalId).fadeIn()

  $(document).on 'click', '.modal-window', (e)->
    e.stopPropagation()


  $(document).on 'click', '.modal-container', (e)->
    $(this).fadeOut()

  $(document).on 'click', 'button.btn-close', (e)->
    $(this).parents('.modal-container').fadeOut()

  $(document).ajaxSuccess (e)->

    console.log e