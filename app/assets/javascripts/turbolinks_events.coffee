$(document).on 'ready', ->

  debug = false

  $(document).on 'turbolinks:load', ->
    console.log 'turbolinks:load' if debug
    $('body').fadeIn('slow')

  $(document).on 'turbolinks:click', ->
    console.log 'turbolinks:click' if debug


  $(document).on 'turbolinks:render', ->
    console.log 'turbolinks:render' if debug
    $('body').hide()
  #    $('body').show('fadeOut')

  $(document).on 'turbolinks:before-visit', ->
    console.log 'turbolinks:before-visit' if debug
    $('body').fadeOut('slow')

  $(document).on 'turbolinks:visit', ->
    console.log 'turbolinks:visit' if debug

  $(document).on 'turbolinks:request-start ', ->
    console.log 'turbolinks:request-start ' if debug


  $(document).on 'turbolinks:request-end', ->
    console.log 'turbolinks:request-end' if debug


  $(document).on 'turbolinks:before-cache', ->
    console.log 'turbolinks:before-cache' if debug

  $(document).on 'turbolinks:before-render', ->
    console.log 'turbolinks:before-render' if debug
#    $('body').hide()