$(document).on 'ready turbolinks:render', ->

  if $('#menu_show_more').length > 0

    top = 0
    index = 0

    menu_items = []
    cur_items = []

    $('.menu .menu-item').each (i, element) ->
      self = $(element)

      self.attr('data-id', i)


      if top != self.offset().top
        top = self.offset().top
        cur_items = menu_items[index++] = []

      cur_items.push
        index: i
        top: self.offset().top
        active: self.hasClass('active')

    console.log menu_items

    if menu_items.length == 1
      $('#menu_show_more').hide()

    active = menu_items[0].find (x) ->
      x.active

    unless active
      i = 1
      while !active and i < menu_items.length
        active = menu_items[i++].find (x) ->
          x.active

      if active
        second_index = active.index - ((i-1) * menu_items[0].length)

        first = $('.menu .menu-item[data-id=' + active.index + ']')
        second = $('.menu .menu-item[data-id=' + (second_index)  + ']')
        second.before(first)


    opened = false

    $(document).on 'click', '#menu_show_more', ->

      self = $(this)

      parent = self.parents('.user-menu-box')
      parent.toggleClass('opened')

      i = 0
      active = null
      while !active  and i < menu_items.length

        active = menu_items[i++].find (x) ->
          x.active

      if active
        first = $('.menu .menu-item[data-id=' + active.index + ']')

      console.log i

      if opened
        opened = false

        if i <= 1
          index = active.index + 1
        else
          index = active.index - ((i-1) * menu_items[0].length)


        second = $('.menu .menu-item[data-id=' + index  + ']')
        second.before(first)

      else
        opened = true

        if i <= 1
          index = active.index + 1 + menu_items[0].length
        else
          index = active.index + 1
        second = $('.menu .menu-item[data-id=' + index  + ']')
        console.log second
        second.before(first)