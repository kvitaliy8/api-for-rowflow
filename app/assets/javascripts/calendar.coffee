$(document).on 'ready turbolinks:render', ->

  $('#fullcalendar').fullCalendar

    customButtons:
      newEvent:
        text: '+'
        click: ->
          $('#event_modal').show()


    height: 750

    header:
      left: 'newEvent'
      center: ''
      right: 'prev,title,next'

    events:
      url: $('#fullcalendar').data('url')
      type: 'POST'

      data:
        authenticity_token: document.querySelector('meta[name="csrf-token"]').content
#        custom_param1: 'something',
#        custom_param2: 'somethingelse'

      error: ->
        alert('there was an error while fetching events!')

      color: '#b7b7b7'
      textColor: '#000000'

    dayClick: (date, jsEvent, view) ->
#      alert('Clicked on: ' + date.format())
#      alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY)
#      alert('Current view: ' + view.name)

    eventClick: (calEvent, jsEvent, view) ->
      modal = $('#edit_event_modal')
      modal.find('[data-modal="title"]').text(calEvent.title)

      startDate = calEvent.start.format('dddd, MMMM Do')
      modal.find('[data-modal="date"]').text(startDate)
#      calEvent.start.format('H a')

      modal.fadeIn()

      console.log calEvent
#      alert('Event: ' + calEvent.title)
#      alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY)
#      alert('View: ' + view.name)



  return
