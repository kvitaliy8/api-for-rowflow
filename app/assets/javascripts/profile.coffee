readURL = (input) ->
  if input.files and input.files[0]
    reader = new FileReader

    reader.onload = (e) ->
      $('.profile-avatar img').attr 'src', e.target.result
      $('.user-avatar a img').attr 'src', e.target.result
      return

    reader.readAsDataURL input.files[0]
  return


$(document).on 'ready', ->

  $(document).on 'click', 'button.change-avatar', (e)->

    if e.originalEvent
      $(this).find('input').click()



  $(document).on 'click', '.editable-data', (e)->

    if e.target.tagName == 'TD'

      self = $(this)

      if self.active
        self.active = false
        self.active = true
      else
        self.active = true
        console.log self.text()

        input = $('<input class="editable" type="' + self.data('type') + '" value="' + self.text() + '" />')
        input.get(0).onblur = ->

          self.text(this.value)

          if self.data('name') == 'first_name' || self.data('name') == 'last_name'
            full_name = $('.profile-info .editable-data[data-name=first_name]').text()
            full_name += ' '
            full_name += $('.profile-info .editable-data[data-name=last_name]').text()
            $('.user-text-content .user-name').text(full_name)
            $('.profile-info-box h3.full-name').text(full_name)


          token = document.querySelector('meta[name="csrf-token"]').content

          data =
            authenticity_token: token

          data[self.data('name')] = this.value

#          form_data.append('authenticity_token', token)

          $.ajax
            type: "PUT"
            url: '/users/update_my_info'
            data: data
      #      success: success

          $(this).remove()


          console.log 433434

        self.html(input)
        input.focus();
        input.get(0).setSelectionRange(input.val().length, input.val().length )





  $(document).on 'change', 'button.change-avatar input', ->

    readURL this

    form_data = new FormData()
    form_data.append('image', this.files[0])

    token = document.querySelector('meta[name="csrf-token"]').content

    form_data.append('authenticity_token', token)

    $.ajax
      type: "PUT"
      url: $(this).data('url')
      processData: false
      data: form_data
      contentType: false
#      success: success
#      dataType: dataType



