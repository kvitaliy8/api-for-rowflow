class OrganizationLinksController < ApplicationController

	def index
		@organization_links = current_user.organization_links.uniq
	end

	def create
		@organization_link = OrganizationLink.new organization_link_params

		unless @organization_link.valid? and @organization_link.save
			@errors = @direct_message.errors
		end

		redirect_to organization_links_path
	end

	def destroy
		@organization_link = OrganizationLink.find params[:id]
		unless @organization_link.destroy
			@errors = @organization_link.errors
		end

		redirect_to organization_links_path
	end

	private

	def organization_link_params
		params.require(:organization_link).permit( :organization_id, :link, :name, :description )
	end

    def set_controller_required_permissions
        @permissions_model = OrganizationLink
        @restricted_actions = [:create, :destroy]
    end
end
