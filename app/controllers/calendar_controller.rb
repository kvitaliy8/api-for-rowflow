class CalendarController < ApplicationController

	def index
		@events = current_user.events
		@upcoming_events = current_user.upcoming_events.group_by(&:month)
	end

	def events
		start_date = DateTime.parse(params[:start])
		end_date = DateTime.parse(params[:end])
    @events = current_user.events.where(occurs_at: start_date..end_date)
	end

	def new
		@event = current_user.events.new
  end

  def show
    @event = current_user.events.find params[:id]
  end

  def create_event
    @event = current_user.events.build event_params
    @event.created_by_user = current_user

		if @event.valid? and @event.save
      EventInvite.create(user_id: current_user.id, event_id: @event.id)
    else
			@errors = @event.errors
    end

    redirect_to calendar_index_path
  end

  def invite
    @event = current_user.events.find params[:id]
    @invite = EventInvite.new invite_params
    if @event.event_invites.map(&:user_id).include? @invite.user_id
      # If already invited, just go back to the event page
      redirect_to calendar_path(@event)
    else
      if @invite.valid?
        @invite.save
      else
        @errors = @invite.errors
      end

      redirect_to calendar_path(@event)
    end
  end

  def rsvp
    @invite = current_user.event_invites.find_by_event_id params[:id]

    unless @invite.update_attributes(rsvp_at: DateTime.now)
      @errors = @invite.errors
    end

    redirect_to calendar_path(params[:id])
  end

	private

  def event_params
    params.require(:event).
      permit(:organization_id, :name, :occurs_at, :location_lat, :location_long,
             :required_points, :alcohol, :venue_name, :venue_address, :venue_city,
             :venue_state, :venue_zipcode, :achievable_points, :coordinator_id, :attachment
      )
  end

  def invite_params
    params.require(:event_invite).permit(:user_id)
  end

  def set_controller_required_permissions
      @permissions_model = Event
      @restricted_actions = [:create]
  end
end
