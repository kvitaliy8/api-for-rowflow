class MessagesController < ApplicationController
	before_action :set_message_group

	def create
		@message = Message.new message_params

		@message.from_user = current_user
		@message.message_group = @message_group

		if @message.valid?
			@message.save

			# If this message is in a direct message, Let the other users in the group know they have a new message
			@message_group.users_now_have_unread_messages_except current_user if @message_group.is_a? DirectMessage
		else
			@errors = @message.errors
		end

		if @message_group.is_a? DirectMessage
			redirect_to direct_message_path(@message_group)
		else
			redirect_to channel_path(@message_group)
		end

	end

	private

	def message_params
		params.require(:message).permit(:message)
	end

	def set_message_group
		requested_message_group_id = params[:channel_id] || params[:direct_message_id]
		if requested_message_group_id.present?
            @message_group = MessageGroup.find(requested_message_group_id)
	    end
	end

end
