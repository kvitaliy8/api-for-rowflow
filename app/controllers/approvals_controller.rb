class ApprovalsController < ApplicationController

	def index
		@tasks = current_user.approvals
	end

    def approve
        @approval = current_user.approvals.find params[:id]

		unless @approval and @approval.update_attributes(approved_at: DateTime.now)
			@errors = @approval.errors
		end
    end

end
