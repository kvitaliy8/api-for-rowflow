class FormSubmissionsController < ApplicationController
	before_action :set_form

	def index
		@form_submissions = @form.form_submissions.where(user_id: current_user.id)
		@form_submission = @form.form_submissions.new
	end

	def create
		@form_submission = @form.form_submissions.build form_submission_params
		@form_submission.user = current_user

		unless @form_submission.valid? and @form_submission.save
			@errors = @form_submission.errors
		end
	end

	private

	def form_submission_params
		params.require(:form_submission).permit(:attachment)
	end

	def set_form
		@form = current_user.forms.find params[:form_id]
	end

end
