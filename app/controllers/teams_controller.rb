class TeamsController < ApplicationController

  def index
    @teams = current_user.organizations.uniq
  end

  def show
    @team = Organization.find params[:id]
    @organization_user_roles = @team.organization_user_roles
    @organization_user_role = OrganizationUserRole.new(organization_id: @team)
  end

  def create
    @team = Organization.new teams_params

    unless @team.valid? and @team.save
      @errors = @team.errors
    else
      sa_role = Role.find_by_title "Super Admin"
      @team.organization_user_roles.find_or_create_by(user: current_user, role: sa_role)
    end

  end

  def update_organization_image
    @team = Organization.find params[:id]
    @team.update(image: params[:image])
  end

  def destroy
  	@team = Organization.find params[:id]

    unless @team.present? and @team.destroy
      @errors = @team.errors
    end

  end

  private

  def teams_params
    params.require(:organization).permit(:name, :organizable)
  end

  def set_controller_required_permissions
    @permissions_model = Organization
    @restricted_actions = [:create, :destroy]
  end

end
