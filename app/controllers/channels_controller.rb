class ChannelsController < ApplicationController

	def index
		@channels = current_user.channels.uniq
  end

  def show
    @channel = current_user.channels.includes(:messages).find(params[:id])
  end

  def create
    @channel = Channel.new channel_params

    if @channel.valid?
      @channel.save
      redirect_to channel_path(@channel)
    else
      @errors = @channel.errors
      redirect_to channels_path
    end
  end

  def destroy
    @channel = Channel.find params[:id]

    unless @channel.present? and @channel.destroy
      @errors = @channel.errors
    end

    redirect_to channels_path
  end

	private

  def channel_params
    params.require(:channel).permit(:name, :organization_id)
  end

  def set_controller_required_permissions
    @permissions_model = Channel
    @restricted_actions = [:create, :destroy]
  end

end
