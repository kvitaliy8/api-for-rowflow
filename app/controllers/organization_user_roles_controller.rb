class OrganizationUserRolesController < ApplicationController
  before_action :set_organization

  def create
    @organization_user_role = @organization.organization_user_roles.build organization_user_role_params

    unless @organization_user_role.valid? and @organization_user_role.save
      @errors = @organization_user_role.errors
    end
  end

  def destroy
    @organization_user_role = OrganizationUserRole.find params[:id]
    unless @organization_user_role.destroy
      @errors = @organization_user_role.errors
    end
  end

  private

  def organization_user_role_params
    params.require(:organization_user_role).permit(:organization_id, :user_id, :role_id)
  end

  def set_organization
    @organization = Organization.find params[:team_id]
  end

end
