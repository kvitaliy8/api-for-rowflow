class DuesTransactionsController < ApplicationController

	def create
		@dues_transaction = current_user.dues_transactions.build dues_transaction_params
		@dues_transaction.created_by_user = current_user
		@dues_transaction.occurred_at = DateTime.now

		unless @dues_transaction.valid? and @dues_transaction.save
			@errors = @dues_transaction.errors
		end

		redirect_to dues_path
	end

	private

	def dues_transaction_params
		params.require(:dues_transaction).permit(:amount, :notes)
	end

end
