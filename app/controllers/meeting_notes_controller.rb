class MeetingNotesController < ApplicationController

	def index
		@meeting_notes = current_user.meeting_notes.uniq
	end

	def show
		@meeting_note = MeetingNote.find params[:id]
	end

    def set_controller_required_permissions
        @permissions_model = MeetingNote
        @restricted_actions = [:create]
    end

end
