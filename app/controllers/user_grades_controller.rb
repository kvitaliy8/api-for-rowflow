class UserGradesController < ApplicationController

	def index
		@user_grades = current_user.user_grades
	end

	def create
		@user_grade = current_user.user_grades.build user_grade_params

		if @user_grade.valid?
			@user_grade.save
		else
			@errors = @user_grade.errors
		end

		redirect_to user_grades_path
	end

	def destroy
		@user_grade = current_user.user_grades.find params[:id]
		unless @user_grade.destroy
			@errors = @user_grade.errors
		end

		redirect_to user_grades_path
	end

	private

	def user_grade_params
		params.require(:user_grade).permit(:course, :grade, :notes, :attachment)
	end
end
