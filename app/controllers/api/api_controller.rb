
class Api::ApiController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :null_session

    respond_to :json

    # Overwrite devise method
    def current_user
        token = request.headers['Authorization'].presence
        if token
    	    @current_user ||= User.find_by_auth_token(token)
        end
    end

    def authenticate_with_token!
    	render json: { errors: "Not authenticated"}, status: :unauthorized unless current_user.present?
    end

    def require_permissions! perms
        render json: {errors: 'User has insufficient permissions.'}, status: :forbidden unless (current_user.present? and perms.all? { |p| current_user.has_permission? p })
    end

    def not_found
        render json: {errors: 'Not found' }, status: 404
    end
    rescue_from ActiveRecord::RecordNotFound, with: :not_found

    def set_user requested_user_id, other_user_permissions
        if requested_user_id == current_user.id
            @user = current_user
        else
            require_permissions! other_user_permissions
            @user = User.find(requested_user_id)
        end
    end

end