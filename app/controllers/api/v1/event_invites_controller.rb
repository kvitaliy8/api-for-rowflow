class Api::V1::EventInvitesController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        @event_invites = current_user.event_invites
    end

    def rsvp
        @event_invite = current_user.event_invites.find(params[:id])

        unless @event_invite.update_attributes(rsvp_at: DateTime.now)
            render json: {errors: @event_invite.errors}, status: :unprocessable_entity
        end
    end

    def check_in
        @event_invite = current_user.event_invites.find(params[:id])

        unless @event_invite.check_in check_in_params[:location_lat], check_in_params[:location_long]
            render json: {errors: @event_invite.errors}, status: :unprocessable_entity
        end
    end

    private

    def check_in_params
        params.permit(:location_lat, :location_long)
    end

end