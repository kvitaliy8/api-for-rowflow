class Api::V1::SessionsController < Api::ApiController
	# Api Session Authentication (log in/ logout)

	def create
		user_email = request.headers['X-API-EMAIL'].presence
		user_password = request.headers['X-API-PASS'].presence
		user_device_token = request.headers['X-API-DEVICE-TOKEN'].presence
		user_device_type = request.headers['X-API-DEVICE-TYPE'].presence


		@user = User.find_by_email(user_email) if user_email.present?

		if @user
			if @user.valid_password?(user_password)
				sign_in @user, store: false
				@user.generate_auth_token
				@user.device_token = user_device_token
				@user.device_type = user_device_type
				@user.save

				render :create, status: :created, formats: [:json]
			else
				render json: {errors: ["Invalid email or password"]}, success: false, status: :unauthorized
			end
		else
			render json: {errors: "User not found"}, status: :unprocessable_entity
		end
	end

	def destroy
		user = User.find_by_auth_token(request.headers['Authorization'])
		if user
			user.generate_auth_token
			user.save
			render json: {message: 'Session Ended'}, success: true, status: :no_content
		else
			render json: {message: 'Invalid Token'}, status: :not_found
		end
	end

end