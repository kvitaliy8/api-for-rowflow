class Api::V1::UserGradesController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        @user_grades = current_user.user_grades
    end

    def show
        @user_grade = current_user.user_grades.find(params[:id])
    end

    def create
        @user_grade = current_user.user_grades.build create_params

        unless @user_grade.valid? and @user_grade.save()
            render json: {errors: @user_grade.errors}, status: :unprocessable_entity
        end

    end

    private

    def create_params
        params.permit(:course, :grade, :notes, :attachment)
    end

end