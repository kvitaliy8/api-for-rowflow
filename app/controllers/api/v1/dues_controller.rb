class Api::V1::DuesController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        @dues = current_user.dues
    end

end