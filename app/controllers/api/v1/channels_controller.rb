class Api::V1::ChannelsController < Api::ApiController
    before_action :authenticate_with_token!

	def index
		@channels = current_user.channels
	end

end
