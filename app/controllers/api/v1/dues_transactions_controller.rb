class Api::V1::DuesTransactionsController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        @dues_transactions = current_user.dues_transactions
    end

    def create
        dt_attrs = create_params
        dt_attrs[:created_by_user_id] = current_user.id
        dt_attrs[:occurred_at] = dt_attrs[:occurred_at] ? Date.parse(dt_attrs[:occurred_at]) : DateTime.now

        @dues_transaction = current_user.dues_transactions.build(dt_attrs)

        unless @dues_transaction.valid? and @dues_transaction.save()
            render json: {errors: @dues_transaction.errors}, status: :unprocessable_entity
        end

    end

    private

    def create_params
        params.permit(:occurred_at, :amount, :notes)
    end

end