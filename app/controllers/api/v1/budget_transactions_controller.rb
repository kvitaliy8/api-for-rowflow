class Api::V1::BudgetTransactionsController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        budget = Budget.find(params[:budget_id])
        @budget_transactions = budget.budget_transactions
    end

    def create
        budget = Budget.find(params[:budget_id])

        bt_attrs = create_params
        bt_attrs[:created_by_user_id] = current_user.id
        bt_attrs[:occurred_at] = bt_attrs[:occurred_at] ? Date.parse(bt_attrs[:occurred_at]) : DateTime.now

        if create_params[:amount] and create_params[:amount].to_i > budget.remaining
            render json: {errors: "Amount greater than remaining amount in budget"}, status: :unprocessable_entity
        end

        @budget_transaction = budget.budget_transactions.build(bt_attrs)

        unless @budget_transaction.valid? and @budget_transaction.save()
            render json: {errors: @budget_transaction.errors}, status: :unprocessable_entity
        end

        # Make sure to deduct the amount of the transaction from the remaining amount in the budget
        budget.subtract_from_remaining! @budget_transaction.amount
    end

    private

    def create_params
        params.permit(:occurred_at, :amount, :notes, :attachment)
    end

end