class Api::V1::ApprovalsController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        # include related object to be approved?
        @approvals = current_user.approvals
    end

    def approve
        @approval = current_user.approvals.find(params[:id])

        unless @approval.update_attributes(approved_at: DateTime.now)
            render json: {errors: @approval.errors}, status: :unprocessable_entity
        end
    end

end