class Api::V1::MessageGroupsUsersController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        # Only pull users for message groups the current user is associated to
        message_group = current_user.direct_messages.find(params[:direct_message_id])
        @message_groups_users = message_group.message_groups_users
    end

    def create
        require_permissions! ['create_message_groups_user'] # need role to add a user to a message group

        message_group = current_user.direct_messages.find(params[:direct_message_id])

        @message_groups_user = message_group.message_groups_users.build(create_params)

        unless @message_groups_user.valid? and @message_groups_user.save()
            render json: {errors: @message_groups_user.errors}, status: :unprocessable_entity
        end

    end

    def update
        message_group = current_user.direct_messages.find(params[:direct_message_id])
        @message_groups_user = message_group.message_groups_users.find(params[:id])

        unless @message_groups_user.update_attributes(update_params)
            render json: {errors: @message_groups_user.errors}, status: :unprocessable_entity
        end
    end

    private

    def create_params
        params.permit(:user_id)
    end

    def update_params
        params.permit(:has_unread_messages)
    end
end