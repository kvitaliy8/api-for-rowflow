class Api::V1::UsersController < Api::ApiController
    before_action :authenticate_with_token!, only: [:index, :show, :update]

    def reset_password
        @user = User.find_by_email(params[:email])
        if @user.present?
            @user.send_reset_password_instructions
            render json: { result: "Email with reset instructions has been sent"}, status: :ok
        else
            render json: { errors: "Email not found"}, status: :unprocessable_entity
        end
    end

    def index
        @users = current_user.directory
    end

    def show
        set_user params[:id].to_i, []
    end

    def update
        set_user params[:id].to_i, other_user_permissions=['create_user']

        update_params[:date_of_birth] = update_params[:date_of_birth] ? Date.parse(update_params[:date_of_birth]) : nil
        unless @user.update_attributes(update_params)
            render json: {errors: @user.errors}, status: :unprocessable_entity
        end
    end

    private

    def update_params
        params.permit(:first_name, :last_name, :password, :image, :cellphone, :email, :clubs_orgs, :facebook_url,
            :instagram_url, :date_of_birth, :major, :hometown, :schedule_url, :previous_semester_gpa, :cumulative_gpa,
            :emergency_contact_name, :emergency_contact_number, :year_in_school, :device_token, :device_type)
    end
end