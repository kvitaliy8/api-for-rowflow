class Api::V1::MeetingNotesController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        @meeting_notes = current_user.meeting_notes
    end

    def show
        @meeting_note = current_user.meeting_notes.find(params[:id])
    end

    def create
    	if not create_params['organization_id'] or not current_user.organization_ids.include? create_params['organization_id'].to_i
    		render json: {errors: "User is not associated to that organization_id"}, status: :unprocessable_entity
      else
        create_params[:occurred_at] = create_params[:occurred_at] ? Date.parse(create_params[:occurred_at]) : DateTime.now
        @meeting_note = MeetingNote.new create_params
    	  @meeting_note.occurred_at = DateTime.now if not create_params['occurred_at']

        unless @meeting_note.valid? and @meeting_note.save()
            render json: {errors: @meeting_note.errors}, status: :unprocessable_entity
        end
    	end
    end

    private

    def create_params
    	params.permit(:organization_id, :notes, :occurred_at, :name, :attachment)
    end

end
