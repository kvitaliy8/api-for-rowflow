class Api::V1::TasksController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        set_user params[:user_id].to_i, other_user_permissions=['view_task']

        @tasks = @user.tasks
    end

    def show
        set_user params[:user_id].to_i, other_user_permissions=['view_task']

        @task = @user.tasks.find(params[:id])
    end

    def create
        # Assign a Task to someone
        set_user params[:user_id].to_i, other_user_permissions=['create_task']

        task_attrs = task_params
        task_attrs[:assigned_to_user_id] = @user.id
        task_attrs[:assigned_by_user_id] = current_user.id
        task_attrs[:due_at] = task_attrs[:due_at] ? Date.parse(task_attrs[:due_at]) : DateTime.now

        @task = @user.tasks.build(task_attrs)

        unless @task.valid? and @task.save()
            render json: {errors: @task.errors}, status: :unprocessable_entity
        end

    end

    def update
        set_user params[:user_id].to_i, other_user_permissions=['create_task']
        @task = @user.tasks.find(params[:id])
        task_params[:due_at] = task_params[:due_at] ? Date.parse(task_params[:due_at]) : DateTime.now

        unless @task.update_attributes task_params
            render json: {errors: @task.errors}, status: :unprocessable_entity
        end
    end

    def complete
        set_user params[:user_id].to_i, other_user_permissions=['create_task']

        @task = @user.tasks.find(params[:id])

        unless @task.update_attributes(completed_at: DateTime.now)
            render json: {errors: @task.errors}, status: :unprocessable_entity
        end
    end

    def incomplete
        set_user params[:user_id].to_i, other_user_permissions=['create_task']

        @task = @user.tasks.find(params[:id])

        unless @task.update_attributes(completed_at: nil)
            render json: {errors: @task.errors}, status: :unprocessable_entity
        end
    end

    private

    def task_params
        params.permit(:due_at, :notes, :task_type, :attachment)
    end

end