class Api::V1::DirectMessagesController < Api::ApiController
    before_action :authenticate_with_token!

	def index
		@direct_messages = current_user.direct_messages
	end

	def create
		require_permissions! ['create_direct_message']

		@direct_message = current_user.direct_messages.build(create_params)

        unless @direct_message.valid? and @direct_message.save()
            render json: {errors: @direct_message.errors}, status: :unprocessable_entity
        end

        # Add the current user to the message group
        @direct_message.message_groups_users.create(user: current_user)
	end

	private

	def create_params
		params.permit(:name)
	end
end
