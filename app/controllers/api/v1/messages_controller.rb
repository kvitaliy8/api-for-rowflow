class Api::V1::MessagesController < Api::ApiController
    before_action :authenticate_with_token!
    before_action :set_message_group

    def index
        @messages = @message_group.messages
    end

    def create
        @message = Message.new create_params
        @message.from_user = current_user
        @message.message_group = @message_group

        unless @message.valid? and @message.save()
            render json: {errors: @message.errors}, status: :unprocessable_entity
        end

        json = render 'api/v1/messages/create'
        $redis.publish('message.create', json)
    end

    private

    def create_params
        params.permit(:message)
    end

    def set_message_group
        # Only pull messages for message groups the current user is associated to
        if params[:channel_id]
            @message_group = current_user.channels.find(params[:channel_id])
        elsif params[:direct_message_id]
            @message_group = current_user.direct_messages.find(params[:direct_message_id])
        else
            render json: {errors: 'No "channel_id" or "direct_message_id" supplied'}, status: :unprocessable_entity
        end
    end
end
