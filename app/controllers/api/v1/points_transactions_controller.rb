class Api::V1::PointsTransactionsController < Api::ApiController
    before_action :authenticate_with_token!

    def index
        set_user params[:user_id].to_i, other_user_permissions=['view_points_transaction']

        @points_transactions = @user.points_transactions
    end

    def create
        set_user params[:user_id].to_i, other_user_permissions=['create_points_transaction']

        pt_attrs = create_params
        pt_attrs[:created_by_user_id] = current_user.id
        pt_attrs[:occurred_at] = pt_attrs[:occurred_at] ? Date.parse(pt_attrs[:occurred_at]) : DateTime.now

        @points_transaction = @user.points_transactions.build(pt_attrs)

        unless @points_transaction.valid? and @points_transaction.save()
            render json: {errors: @points_transaction.errors}, status: :unprocessable_entity
        end

    end

    private

    def create_params
        params.permit(:occurred_at, :amount, :notes)
    end

end