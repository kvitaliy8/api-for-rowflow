class Api::V1::BudgetsController < Api::ApiController
    before_action :authenticate_with_token!

	def index
		@budgets = current_user.budgets
	end

end
