class DuesController < ApplicationController

	def index
		@dues = current_user.dues.uniq
		@dues_transactions = current_user.dues_transactions
	end

	private

    def set_controller_required_permissions
        @permissions_model = Due
        @restricted_actions = [:create]
    end

end
