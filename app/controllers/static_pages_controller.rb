class StaticPagesController < ApplicationController

	def index
		unless user_signed_in?
			redirect_to new_user_session_path
		else
			render :home, layout: false
		end
	end

	def home
		render :home, layout: false
	end



	def page

		p params[:page]

		render params[:page]

	end















end
