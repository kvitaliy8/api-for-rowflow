class BirthdaysController < ApplicationController

	def index
		@birthdays = current_user.directory.map do |user|
			{name: user.full_name, birthday: user.birthday}
		end
	end

end