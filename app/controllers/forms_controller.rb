class FormsController < ApplicationController

	def index
		@forms = current_user.forms.uniq
	end

    def set_controller_required_permissions
        @permissions_model = Form
        @restricted_actions = [:create]
    end

end
