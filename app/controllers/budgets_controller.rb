class BudgetsController < ApplicationController

	def index
		@budgets = current_user.budgets.uniq
	end

	def show
		@budget = Budget.find params[:id]

		unless current_user.budgets.include? @budget
			raise ActiveRecord::RecordNotFound
		end

		@user_can_view_transactions = current_user.can_view? BudgetTransaction

		if @user_can_view_transactions
			@expenses = @budget.budget_transactions
		else
			@expenses = current_user.expenses.where(transactable: @budget)
		end
	end

	private

    def set_controller_required_permissions
        @permissions_model = Budget
        @restricted_actions = [:create]
    end

end
