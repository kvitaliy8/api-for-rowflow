class BudgetTransactionsController < ApplicationController

    def create
        @budget = Budget.find(params[:budget_id])

        @budget_transaction =  @budget.budget_transactions.build create_params
        @budget_transaction.created_by_user = current_user

        # if create_params[:amount] and create_params[:amount].to_i > budget.remaining
        #     render json: {errors: "Amount greater than remaining amount in budget"}, status: :unprocessable_entity
        # end

        unless @budget_transaction.valid? and @budget_transaction.save()
            @errors = @budget_transaction.errors
            return
        end

        # Make sure to deduct the amount of the transaction from the remaining amount in the budget
        @budget.subtract_from_remaining! @budget_transaction.amount

        redirect_to budget_path(@budget)
    end

    private

    def create_params
        params.require(:budget_transaction).permit(:occurred_at, :amount, :notes, :attachment)
    end

end