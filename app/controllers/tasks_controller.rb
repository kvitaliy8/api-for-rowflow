class TasksController < ApplicationController

	def index
		@tasks = current_user.tasks
		@my_tasks = current_user.created_tasks
		@task = Task.new
	end

	def create
		@task = Task.new task_params

		@task.assigned_by_user = current_user

		if @task.valid?
			@task.save
		else
			@errors = @task.errors
		end

		p @errors

		redirect_to index
	end

	def complete
		@task = current_user.tasks.find(params[:id])
		unless @task and @task.update_attributes(completed_at: DateTime.now)
			@errors = @task.errors
		else
			# Create an approval for the person who assigned the task to approve that it is complete
			@task.approvals.create(approver: @task.assigned_by_user)
		end
	end

	private

	def task_params
		params.require(:task).permit(:assigned_to_user_id, :notes, :due_at, :attachment)
	end

	def set_controller_required_permissions
		@permissions_model = Task
		@restricted_actions = [:create]
	end
end
