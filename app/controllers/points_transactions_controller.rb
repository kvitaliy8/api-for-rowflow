class PointsTransactionsController < ApplicationController

	def index
		@points = current_user.points_transactions
	end

	def create
		@point = PointsTransaction.new point_params

		@point.transactable = current_user
		@point.created_by_user = current_user
		@point.occurred_at = DateTime.now

		if @point.valid?
			@point.save
		else
			@errors = @point.errors
		end

		redirect_to points_transactions_path
	end

	private

	def point_params
		params.require(:points_transaction).permit(:amount, :notes)
	end

    def set_controller_required_permissions
        @permissions_model = PointsTransaction
        @restricted_actions = [:create]
    end
end
