class ApplicationController < ActionController::Base

	protect_from_forgery with: :exception
	respond_to :html

	before_action :authenticate_user!
	before_action :set_controller_required_permissions
	before_action :check_admin_role
	before_action :configure_permitted_parameters, if: :devise_controller?

	layout :set_layout

	def check_admin_role
		if user_signed_in? and current_user.permissions.include? 'admin'
			@admin = true
		end
	end

	rescue_from ActiveRecord::RecordNotFound do |exception|
		flash[:error] = exception.message
		redirect_back(fallback_location: root_path)
    end

	private

	def set_layout
		devise_controller? ? 'login' : 'application'
	end

	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:account_update, keys: [:image])
	end


	################# PERMISSIONS #########################

    class PermissionsError < StandardError
    end

    rescue_from PermissionsError do |exception|
		flash[:error] = exception.message
		redirect_back(fallback_location: root_path)
    end

	def set_controller_required_permissions
		@permissions_model = nil
		@restricted_actions = []
	end

	before_action :current_user_can_create, only: [:create]
	def current_user_can_create
		if @permissions_model and @restricted_actions.include? :create
			unless current_user.can_create? @permissions_model
				raise PermissionsError.new("You do not have permission to create this #{@permissions_model.name}.");
			end
		end
	end

	before_action :current_user_can_view, only: [:show]
	def current_user_can_view
		if @permissions_model and @restricted_actions.include? :show
			unless current_user.can_view? @permissions_model
				raise PermissionsError.new("You do not have permission to view this #{@permissions_model.name}.");
			end
		end
	end

	before_action :current_user_can_edit, only: [:update]
	def current_user_can_edit
		if @permissions_model and @restricted_actions.include? :update
			unless current_user.can_edit? @permissions_model
				raise PermissionsError.new("You do not have permission to edit this #{@permissions_model.name}.");
			end
		end
	end

	before_action :current_user_can_delete, only: [:destroy]
	def current_user_can_delete
		if @permissions_model and @restricted_actions.include? :destroy
			unless current_user.can_delete? @permissions_model
				raise PermissionsError.new("You do not have permission to delete this #{@permissions_model.name}.");
			end
		end
	end

end
