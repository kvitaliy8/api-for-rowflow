class DirectMessagesController < ApplicationController

	def index
		@direct_messages = current_user.direct_messages
	end

	def show
		@direct_message = DirectMessage.find params[:id]
		unless @direct_message.users.include? current_user
			raise ActiveRecord::RecordNotFound
		end

		# Set that the current user has viewed the message group and no longer has missed messages
		@direct_message.user_has_viewed_message_group current_user
	end

	def create
		@direct_message = DirectMessage.new(direct_message_params)
		@direct_message.add_user current_user

		if @direct_message.valid? and @direct_message.message_groups_users.any?
			@direct_message.save
			redirect_to direct_message_path(@direct_message)
		else
			@errors = @direct_message.errors
			redirect_to direct_messages_path
		end
	end

	def destroy
		# Leave Direct Message Room
	end

	private

	def direct_message_params
		params.require(:direct_message).permit( message_groups_users_attributes: [:user_id] )
	end
end
