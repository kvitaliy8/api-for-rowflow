class UsersController < ApplicationController

	def my_flow
		@tasks = current_user.tasks.includes(:assigned_by_user)
	end

	def index
		@users = current_user.directory
	end

	def show
		@user = User.find params[:id]
	end

	def my_info

	end


	######

	def new
		@user = User.new
	end

	def create
	    @user = User.new user_params

		@errors = @user.errors unless @user.save
	end

	def edit
		@user = User.find params[:id]
	end

	def update
		@user = User.find params[:id]

		unless @user and @user.update user_params
			@errors = @user.errors
		end
	end

	def destroy
		@user = User.find params[:id]

		unless @user and @user.destroy
			@errors = @user.errors
		end
	end

	def update_my_avatar
		current_user.update(image: params[:image])
	end

	def update_my_info
		current_user.update(
      params.permit(
        :hometown,
        :last_name,
        :first_name, :cellphone,
        :date_of_birth, :major
      )
		)
	end

	private

	def user_params
		params.require(:user).permit(:email, :password, :first_name, :last_name, :image)
	end

	def set_controller_required_permissions
		@permissions_model = User
		@restricted_actions = [:create, :update, :destroy]
	end

end
