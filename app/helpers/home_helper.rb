module HomeHelper

  def home_menu_item(path, svg_name, title, num_count)
    locals = {
        menu_item_path: path,
        svg_name: svg_name,
        title: title,
        num_count: num_count
    }
    render partial: 'menu_item', locals: locals
  end

end
