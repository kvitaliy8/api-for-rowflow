module ApplicationHelper

  def menu_item(path, svg_name, title, num_count, action = '')
    locals = {
        menu_item_path: path,
        svg_name: svg_name,
        title: title,
        num_count: num_count,
        actions: action.split(','),
    }
    render partial: 'menu_item', locals: locals
  end

  def display_date(date_object)
    if date_object.present?
      date_object.strftime('%_m/%_d/%y')
    else
      ''
    end
  end

  def display_attachment(attachment_object)
    if attachment_object.exists?
      return link_to attachment_object.url, target: :blank do
        svg_icon 'form_green'
      end
    else
      return svg_icon 'form_grey'
    end
  end

end
