module SvgSpriteHelper

  def svg_icon(filename)
    @svg_icons ||= {}
    @svg_icons[filename] = filename
    ('<svg class="icon"><use xlink:href="#' + filename + '"/></svg>').html_safe
  end

  def render_svg_item(svg_path)
    svg = render file: "_svg_assets/#{svg_path}", formats: [:svg]
    svg.gsub!(/<(\/)?(svg).*?( viewBox="[\s\d]+")?>/, '<\1symbol\3>')
    svg.sub!(/(<symbol)/, '<symbol id="' + svg_path + '" ')
    svg.gsub!(/\.([A-z]+-\d)(\{|,)/, '.\1-' + svg_path + '\2 ')
    svg.gsub!(/class="([A-z0-9_-]+)"/, 'class="\1-' + svg_path + '"')
  end

  def render_svg_sprite
    @svg_icons ||= {}
    sprite = @svg_icons.keys.map do |svg_icon|
      render_svg_item(svg_icon)
    end
    ('<svg style="display: none; visibility: hidden">' + sprite.join("\n") + '</svg>').html_safe
  end

end
