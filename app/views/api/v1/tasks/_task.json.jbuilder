json.extract! task, :id, :assigned_to_user_id, :due_at, :notes, :task_type, :completed_at, :created_at
json.attachment task.attachment.url
