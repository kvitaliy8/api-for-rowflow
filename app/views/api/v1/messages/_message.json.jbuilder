json.extract! message, :id, :from_user_id, :message_group_id, :message, :created_at
json.user do
	json.first_name message.from_user.first_name
	json.last_name message.from_user.last_name
	json.image message.from_user.image.url
end