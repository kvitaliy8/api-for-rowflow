json.extract! user, :id, :email, :first_name, :last_name, :cellphone, :facebook_url, :instagram_url, :date_of_birth, :major, :clubs_orgs,
					:hometown, :schedule_url, :previous_semester_gpa, :cumulative_gpa, :emergency_contact_name, :emergency_contact_number, :year_in_school, :created_at
json.image user.image.url
json.permissions user.permissions
json.organizations user.organizations.map do |org|
	json.id org.id
	json.name org.name
end