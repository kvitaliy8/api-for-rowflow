json.extract! user_grade, :id, :user_id, :course, :grade, :notes, :created_at
json.attachment user_grade.attachment.url
