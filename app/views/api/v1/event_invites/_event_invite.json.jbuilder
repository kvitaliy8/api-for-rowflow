json.extract! event_invite, :id, :event_id, :user_id, :rsvp_at, :checked_in_at, :created_at
json.extract! event_invite.event, :name, :occurs_at, :required_points, :alcohol, :venue_name, :venue_address, :venue_city, :venue_state, :venue_zipcode, :achievable_points
