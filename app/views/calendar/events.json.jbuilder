json.array! @events do |event|
  json.(event, :id)
  json.title event.name
  json.allDay true
  json.start event.occurs_at.utc.iso8601
  json.className 'test'

  # json.color 'red'
  # json.borderColor 'green'
  # json.textColor 'black'

  # next if comment.marked_as_spam_by?(current_user)

  # json.body comment.body
  # json.author do
  #   json.first_name comment.author.first_name
  #   json.last_name comment.author.last_name
  # end
end


# end
# url
# className
# editable
# startEditable
# durationEditable
# resourceEditable
# rendering
# overlap
# constraint
# source
